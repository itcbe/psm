﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Maintenance_frequency_services
    {
        public List<MaintenanceFrequency> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from mf in entities.MaintenanceFrequency
                        where mf.Active == true
                        orderby mf.Frequency
                        select mf).ToList();
            }
        }

        public ObservableCollection<MaintenanceFrequency> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<MaintenanceFrequency>((from mf in entities.MaintenanceFrequency
                                                                       where mf.Active == true
                                                                       orderby mf.Frequency
                                                                       select mf).ToList());
            }
        }

        public MaintenanceFrequency Save(MaintenanceFrequency frequency)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from mf in entities.MaintenanceFrequency
                             where mf.Frequency == frequency.Frequency
                             select mf).FirstOrDefault();

                if (found == null)
                {
                    entities.MaintenanceFrequency.Add(frequency);
                    entities.SaveChanges();

                    return frequency;
                }
                else
                    throw new Exception("De onderhoudsfrequentie bestaat reeds!");
            }
        }

        public void Update(MaintenanceFrequency frequency)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from mf in entities.MaintenanceFrequency
                             where mf.Id == frequency.Id
                             select mf).FirstOrDefault();

                if (found != null)
                {
                    found.Frequency = frequency.Frequency;
                    found.Active = frequency.Active;
                    found.Modified = frequency.Modified;
                    found.ModifiedBy = frequency.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                    throw new Exception("De onderhoudsfrequentie is niet gevonden!");
            }
        }

        public void Delete(MaintenanceFrequency frequency)
        {
            frequency.Active = false;
            Update(frequency);
        }

        public MaintenanceFrequency GetById(int? id)
        {
            using (var entities = new PoortserviceEntities())
            {
                if (id != null)
                    return (from f in entities.MaintenanceFrequency
                            where f.Id == (int)id
                            select f).FirstOrDefault();
                else
                    return null;
            }
        }
    }
}
