﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class GateBrand_services
    {
        public List<GateBrand> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from b in entities.GateBrand
                        where b.Active == true
                        orderby b.Brand
                        select b).ToList();
            }
        }
    }
}
