﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class City_services
    {
        public List<City> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from c in entities.City.Include("Country1")
                        where c.Active == true
                        && c.Country1.Name == "België"
                        orderby c.Name
                        select c).ToList();
            }
        }

        public ObservableCollection<City> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<City>((from c in entities.City.Include("Country1")
                        where c.Active == true
                        && c.Country1.Name == "België"
                        select c).ToList());
            }
        }

        public City Save(City city)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from c in entities.City.Include("Country1")
                        where c.Name.ToLower() == city.Name.ToLower()
                        && c.Zipcode == city.Zipcode
                        select c).FirstOrDefault();

                if (found == null)
                {
                    entities.City.Add(city);
                    entities.SaveChanges();
                    return (from c in entities.City.Include("Country1")
                            where c.Id == city.Id
                            select c).FirstOrDefault();
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        entities.SaveChanges();
                        return (from c in entities.City.Include("Country1")
                                where c.Id == found.Id
                                select c).FirstOrDefault();
                    }
                    else
                        throw new Exception("De gemeente bestaat reeds!");
                }
            }
        }

        public void Update(City city)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from c in entities.City.Include("Country1")
                             where c.Id == city.Id
                             select c).FirstOrDefault();

                if (found != null)
                {
                    found.Active = city.Active;
                    found.Name = city.Name;
                    found.Zipcode = city.Zipcode;
                    found.Country = city.Country;
                    found.Modified = city.Modified;
                    found.ModifiedBy = city.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De gemeente is niet gevonden!");
                }
            }

        }

        public void Delete(City city)
        {
            city.Active = false;
            Update(city);
        }

        public List<City> GetByZipcode(string zipcode)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from c in entities.City where c.Zipcode == zipcode orderby c.Name select c).ToList();
            }
        }
    }
}
