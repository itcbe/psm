//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSMData
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerGate_ServiceInstallation
    {
        public int Id { get; set; }
        public int CustomerGate { get; set; }
        public int ServiceInstallation { get; set; }
        public Nullable<bool> Active { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
    
        public virtual CustomerGate CustomerGate1 { get; set; }
        public virtual ServiceInstallation ServiceInstallation1 { get; set; }
    }
}
