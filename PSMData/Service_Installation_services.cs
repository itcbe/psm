﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Service_Installation_services
    {
        public List<ServiceInstallation> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from sm in entities.ServiceInstallation
                        where sm.Active == true
                        orderby sm.Name
                        select sm).ToList();
            }
        }

        public ServiceInstallation GetByName(string name)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from s in entities.ServiceInstallation
                        where s.Name == name
                        select s).FirstOrDefault();
            }
        }

        public ObservableCollection<ServiceInstallation> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<ServiceInstallation>((from sm in entities.ServiceInstallation
                                                                      where sm.Active == true
                                                                      orderby sm.Name
                                                                      select sm).ToList());
            }
        }

        public ServiceInstallation Save(ServiceInstallation installation)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from s in entities.ServiceInstallation
                             where s.Name.ToLower() == installation.Name.ToLower()
                        select s).FirstOrDefault();

                if (found == null)
                {
                    entities.ServiceInstallation.Add(installation);
                    entities.SaveChanges();
                    return installation;
                }
                else
                {
                    throw new Exception("De bedieningsinstallatie bestaat reeds!");
                }
            }
        }

        public void Update(ServiceInstallation installation)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from s in entities.ServiceInstallation
                             where s.Id == installation.Id
                             select s).FirstOrDefault();

                if (found != null)
                {
                    found.Name = installation.Name;
                    found.Active = installation.Active;
                    found.Modified = installation.Modified;
                    found.ModifiedBy = installation.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De bedieningsinstallatie is niet gevonden!");
                }
            }
        }

        public void Delete(ServiceInstallation installation)
        {
            installation.Active = false;
            Update(installation);
        }

        public List<CustomerGate_ServiceInstallation> GetByGate(int id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from s in entities.CustomerGate_ServiceInstallation.Include("ServiceInstallation1")
                        where s.CustomerGate == id
                        select s).ToList();

            }
        }
    }
}
