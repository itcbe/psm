﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public partial class Intervention
    {
        public string WaitForFeedBackNL 
        {
            get
            {
                return WaitForFeedBack == true ? "Ja": "Nee";
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Intervention)
            {
                Intervention toCompare = (Intervention)obj;
                if (this.CustomerGate != toCompare.CustomerGate)
                    return false;
                if (this.InterventionType != toCompare.InterventionType)
                    return false;
                if (this.PlannedDate != toCompare.PlannedDate)
                    return false;
                if (this.ExecutionDate != toCompare.ExecutionDate)
                    return false;
                if (this.Description != toCompare.Description)
                    return false;
                if (this.Remarks != toCompare.Remarks)
                    return false;
                if (this.WaitForFeedBack != toCompare.WaitForFeedBack)
                    return false;
                
                return true;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
