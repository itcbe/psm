﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public partial class CustomerGate
    {
        public string MaintenanceContractNL 
        {
            get
            {
                return MaintenanceContract == true ? "Ja" : "Nee";
            }

        }

        public override bool Equals(object obj)
        {
            if (obj is CustomerGate)
            {
                CustomerGate toCompare = (CustomerGate)obj;
                if (this.Customer != toCompare.Customer)
                    return false;
                if (this.GateModel != toCompare.GateModel)
                    return false;
                if (this.Quantity != toCompare.Quantity)
                    return false;
                if (this.GateProductionNumber != toCompare.GateProductionNumber)
                    return false;
                if (this.GateColor != toCompare.GateColor)
                    return false;
                if (this.EngineType != toCompare.EngineType)
                    return false;
                if (this.EngineProductionNumber != toCompare.EngineProductionNumber)
                    return false;
                if (this.MaintenanceContract != toCompare.MaintenanceContract)
                    return false;
                if (this.MaintenanceFrequency != toCompare.MaintenanceFrequency)
                    return false;
                if (this.Measurement != toCompare.Measurement)
                    return false;
                if (this.GateDecoration != toCompare.GateDecoration)
                    return false;
                if (this.RailSystem != toCompare.RailSystem)
                    return false;
                if (this.InstallationDate != toCompare.InstallationDate)
                    return false;
                if (this.Voltage != toCompare.Voltage)
                    return false;
                if (this.GateDriver != toCompare.GateDriver)
                    return false;
                if (this.RemoteControl != toCompare.RemoteControl)
                    return false;
                if (this.InvoiceNumber != toCompare.InvoiceNumber)
                    return false;
                if (this.InvoiceDate != toCompare.InvoiceDate)
                    return false;
                if (this.PriceMaintenance != toCompare.PriceMaintenance)
                    return false;

                return true;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
