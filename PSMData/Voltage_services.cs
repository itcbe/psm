﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Voltage_services
    {
        public List<Voltage> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from v in entities.Voltage
                        where v.Active == true
                        orderby v.Amount
                        select v).ToList();
            }
        }

        public ObservableCollection<Voltage> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Voltage>((from v in entities.Voltage
                        where v.Active == true
                        orderby v.Amount
                        select v).ToList());
            }
        }

        public Voltage Save(Voltage voltage)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from v in entities.Voltage
                        where v.Amount == voltage.Amount
                        select v).FirstOrDefault();

                if (found == null)
                {
                    entities.Voltage.Add(voltage);
                    entities.SaveChanges();
                    return voltage;
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        entities.SaveChanges();
                        return found;
                    }
                    else
                        throw new Exception("De aansluitspanning bestaat reeds!");
                }
            }
        }

        public void Update(Voltage voltage)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from v in entities.Voltage
                             where v.Id == voltage.Id
                             select v).FirstOrDefault();

                if (found != null)
                {
                    found.Amount = voltage.Amount;
                    found.Active = voltage.Active;
                    found.Modified = voltage.Modified;
                    found.ModifiedBy = voltage.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De aansluitspanning is niet gevonden!");
                }
            }
        }

        public void Delete(Voltage voltage)
        {
            voltage.Active = false;
            Update(voltage);
        }
    }
}
