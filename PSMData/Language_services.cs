﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Language_services
    {
        public List<Language> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from l in entities.Language
                        where l.Active == true
                        orderby l.Name
                        select l).ToList();
            }
        }

        public ObservableCollection<Language> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Language>((from l in entities.Language
                                                           where l.Active == true
                                                           orderby l.Name
                                                           select l).ToList());
            }
        }

        public Language Save(Language language)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from l in entities.Language
                        where l.Name.ToLower() == language.Name.ToLower()
                        select l).FirstOrDefault();
                if (found == null)
                {
                    entities.Language.Add(language);
                    entities.SaveChanges();
                    return language;
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        return found;
                    }
                    else
                        throw new Exception("De Taal bestaat reeds in de lijst!");
                }
            }
        }

        public void Update(Language language)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from l in entities.Language
                             where l.Id == language.Id
                             select l).FirstOrDefault();

                if (found != null)
                {
                    found.Name = language.Name;
                    found.Code = language.Code;
                    found.Active = language.Active;
                    found.Modified = language.Modified;
                    found.ModifiedBy = language.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De taal is niet gevonden!");
                }
            }
        }

        public void Delete(Language language)
        {
            language.Active = false;
            Update(language);
        }

        public Language GetByCode(string lan)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from l in entities.Language
                        where l.Code == lan
                        select l).FirstOrDefault();
            }
        }
    }
}
