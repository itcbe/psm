﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class InterventionType_services
    {
        public List<InterventionType> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from it in entities.InterventionType
                        where it.Active == true
                        orderby it.Name
                        select it).ToList();
            }
        }

        public ObservableCollection<InterventionType> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<InterventionType>((from it in entities.InterventionType
                                                                   where it.Active == true
                                                                    orderby it.Name
                                                                    select it).ToList());
            }
        }

        public void Delete(InterventionType type)
        {
            type.Active = false;
            Update(type);
        }

        public InterventionType Save(InterventionType type)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from it in entities.InterventionType
                             where it.Name.ToLower() == type.Name.ToLower()
                             select it).FirstOrDefault();

                if (found == null)
                {
                    entities.InterventionType.Add(type);
                    entities.SaveChanges();
                    return type;
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        entities.SaveChanges();
                        return found;
                    }
                    else
                        throw new Exception("Interventietype bestaat reeds!");
                }
            }
        }

        public void Update(InterventionType type)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from it in entities.InterventionType
                             where it.Id == type.Id
                             select it).FirstOrDefault();

                if (found != null)
                {
                    found.Name = type.Name;
                    found.Active = type.Active;
                    found.Modified = type.Modified;
                    found.ModifiedBy = type.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("Interventietype is niet gevonden!");
                }
            }
        }
    }
}
