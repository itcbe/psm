﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public partial class City
    {
        public string FullCity
        {
            get
            {
                return Zipcode + "  " + Name;
            }
        }

        public List<City> AllCities
        {
            get
            {
                City_services cService = new City_services();
                return cService.GetAll();
            }
        }
    }
}
