﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class EngineType_services
    {
        public List<EngineType> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from e in entities.EngineType
                        where e.Active == true
                        orderby e.Name
                        select e).ToList();
            }
        }

        public EngineType Save(EngineType enginetype)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from e in entities.EngineType
                             where e.Name.ToLower() == enginetype.Name.ToLower()
                        select e).FirstOrDefault();

                if (found == null)
                {
                    entities.EngineType.Add(enginetype);
                    entities.SaveChanges();
                    return enginetype;
                }
                else
                    throw new Exception("Motortype bestaat reeds!");
            }
        }

        public void Update(EngineType enginetype)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from e in entities.EngineType
                             where e.Name.ToLower() == enginetype.Name.ToLower()
                             select e).FirstOrDefault();

                if (found != null)
                {
                    found.Name = enginetype.Name;
                    found.Active = enginetype.Active;
                    found.Modified = enginetype.Modified;
                    found.ModifiedBy = enginetype.ModifiedBy;
                    entities.SaveChanges();
                }
            }
        }

        public void Delete(EngineType enginetype)
        {
            enginetype.Active = false;
            Update(enginetype);
        }

        public ObservableCollection<EngineType> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<EngineType>((from e in entities.EngineType
                                                             where e.Active == true
                                                             orderby e.Name
                                                             select e).ToList());
            }
        }
    }
}
