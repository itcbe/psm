﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Template_service
    {
        public List<Template> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from t in entities.Template
                        where t.Active == true
                        select t).ToList();
            }
        }

        public ObservableCollection<Template> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Template>((from t in entities.Template
                                                           where t.Active == true
                                                           select t).ToList());
            }
        }

        public Template Save(Template template)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from t in entities.Template
                             where t.TemplateType.ToLower() == template.TemplateType.ToLower()
                             select t).FirstOrDefault();
                if (found == null)
                {
                    entities.Template.Add(template);
                    entities.SaveChanges();
                    return template;
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        entities.SaveChanges();
                        return found;
                    }
                    else
                        throw new Exception("Template bestaat reeds!");
                }
            }
        }

        public void Update(Template template)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from t in entities.Template
                             where t.Id == template.Id
                             select t).FirstOrDefault();
                if (found != null)
                {
                    found.Active = template.Active;
                    found.TemplateType = template.TemplateType;
                    found.Modified = template.Modified;
                    found.ModifiedBy = template.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("Template is niet gevonden!");
                }
            }
        }

        public void Delete(Template template)
        {
            template.Active = false;
            Update(template);
        }
    }
}
