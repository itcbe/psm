﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Template_Document_service
    {
        public List<Template_Document> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from t in entities.Template_Document.Include("Template1").Include("Document1")
                        where t.Active == true
                        select t).ToList();
            }
        }

        public ObservableCollection<Template_Document> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Template_Document>((from t in entities.Template_Document.Include("Template1").Include("Document1")
                                                                    where t.Active == true
                                                                    select t).ToList());
            }
        }

        public void Save(Template_Document template)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from t in entities.Template_Document.Include("Document1").Include("Document1.Language1")
                             where t.Template == template.Template
                             && t.Document1.Language == template.Document1.Language
                             select t).FirstOrDefault();

                if (found != null)
                {
                    if (found.Document1.Url != template.Document1.Url)
                    {
                        Delete(found);
                        template.Document1 = null;
                        template.Active = true;
                        entities.Template_Document.Add(template);
                        entities.SaveChanges();
                    }
                }
                else
                {
                    template.Document1 = null;
                    template.Active = true;
                    entities.Template_Document.Add(template);
                    entities.SaveChanges();
                }
            }
        }

        private void Delete(Template_Document template)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from t in entities.Template_Document.Include("Document1")
                             where t.Id == template.Id
                             select t).FirstOrDefault();

                if (found != null)
                {
                    entities.Template_Document.Remove(found);
                    entities.SaveChanges();
                }
            }
        }

        public List<Template_Document> GetByTemplate(Template template)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from d in entities.Template_Document.Include("Document1").Include("Document1.Language1")
                        where d.Active == true
                        && d.Template == template.Id
                        select d).ToList();
            }
        }

        public void UpdateDocument(Template_Document temp_doc)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from d in entities.Document
                             where d.Id == temp_doc.Document
                             select d).FirstOrDefault();

                if (found != null)
                {
                    found.Url = temp_doc.Document1.Url;
                    found.Modified = temp_doc.Modified;
                    found.ModifiedBy = temp_doc.ModifiedBy;

                    entities.SaveChanges();
                }
            }
        }

        public Template_Document GetByTemplateAndLanguage(Template template, Language language)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from t in entities.Template_Document.Include("Document1").Include("Document1.Language1")
                        where t.Template == template.Id
                        && t.Document1.Language == language.Id
                        select t).FirstOrDefault();
            }
        }
    }
}
