﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class SafetyMeasure_services
    {
        public List<SafetyMeasure> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from sm in entities.SafetyMeasure
                        where sm.Active == true
                        orderby sm.Name
                        select sm).ToList();
            }
        }

        public SafetyMeasure GetByName(string name)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from sm in entities.SafetyMeasure
                        where sm.Name == name
                        select sm).FirstOrDefault();
            }
        }

        public ObservableCollection<SafetyMeasure> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<SafetyMeasure>((from sm in entities.SafetyMeasure
                                                                where sm.Active == true
                                                                orderby sm.Name
                                                                select sm).ToList());
            }
        }

        public SafetyMeasure Save(SafetyMeasure safety)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from sm in entities.SafetyMeasure
                             where sm.Name.ToLower() == safety.Name.ToLower()
                             select sm).FirstOrDefault();

                if (found == null)
                {
                    entities.SafetyMeasure.Add(safety);
                    entities.SaveChanges();

                    return safety;
                }
                else
                {
                    throw new Exception("De veiligheidsmaatregel bestaat reeds!");
                }
            }
        }

        public void Update(SafetyMeasure safety)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from sm in entities.SafetyMeasure
                             where sm.Id == safety.Id
                             select sm).FirstOrDefault();

                if (found != null)
                {
                    found.Name = safety.Name;
                    found.Active = safety.Active;
                    found.Modified = safety.Modified;
                    found.ModifiedBy = safety.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De veiligheidsmaatregel is niet gevonden!");
                }
            }
        }

        public void Delete(SafetyMeasure safety)
        {
            safety.Active = false;
            Update(safety);
        }

        public List<CustomerGate_SafetyMeasure> GetByGate(int id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from s in entities.CustomerGate_SafetyMeasure.Include("SafetyMeasure1")
                        where s.CustomerGate == id
                        select s).ToList();
            }
        }
    }
}
