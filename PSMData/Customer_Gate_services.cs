﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Customer_Gate_services
    {
        public ObservableCollection<CustomerGate> GetByCustomer(int customer_Id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<CustomerGate>((from cg in entities.CustomerGate.Include("EngineType1").Include("GateModel1").Include("Customer1").Include("GateDecoration1").Include("GateDriver1").Include("MaintenanceFrequency1").Include("RailSystem1").Include("RemoteControl1").Include("Voltage1")
                        where cg.Active == true
                        && cg.Customer == customer_Id
                        select cg).ToList());
            }
        }

        public CustomerGate GetById(int id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from cg in entities.CustomerGate.Include("EngineType1").Include("GateModel1").Include("Customer1").Include("GateDecoration1").Include("GateDriver1").Include("MaintenanceFrequency1").Include("RailSystem1").Include("RemoteControl1").Include("Voltage1")
                        where cg.Active == true
                        && cg.Id == id
                        select cg).FirstOrDefault();
            }
        }

        public CustomerGate Save(CustomerGate gate)
        {
            using (var entities = new PoortserviceEntities())
            {
                //var found = (from cg in entities.CustomerGate
                //             where cg.Customer == gate.Customer
                //             && cg.GateModel == gate.GateModel

                //             select cg).FirstOrDefault();

                //if (found == null)
                //{
                    entities.CustomerGate.Add(gate);
                    entities.SaveChanges();
                    return (from cg in entities.CustomerGate.Include("EngineType1").Include("GateModel1").Include("Customer1").Include("GateDecoration1").Include("GateDriver1").Include("MaintenanceFrequency1").Include("RailSystem1").Include("RemoteControl1").Include("Voltage1")
                        where cg.Active == true
                        && cg.Id == gate.Id
                        select cg).FirstOrDefault();
                //}
                //else
                //    throw new Exception("Poort klant combinatie bestaat reeds!");
            }
        }

        public void Update(CustomerGate gate)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from g in entities.CustomerGate
                             where g.Id == gate.Id
                             select g).FirstOrDefault();

                if (found != null)
                {
                    found.GateModel = gate.GateModel;
                    found.Quantity = gate.Quantity;
                    found.GateProductionNumber = gate.GateProductionNumber;
                    found.GateColor = gate.GateColor;
                    found.EngineType = gate.EngineType;
                    found.EngineProductionNumber = gate.EngineProductionNumber;
                    found.MaintenanceContract = gate.MaintenanceContract;
                    found.MaintenanceFrequency = gate.MaintenanceFrequency;
                    found.Measurement = gate.Measurement;
                    found.GateDecoration = gate.GateDecoration;
                    found.RailSystem = gate.RailSystem;
                    found.InstallationDate = gate.InstallationDate;
                    found.Voltage = gate.Voltage;
                    found.GateDriver = gate.GateDriver;
                    found.RemoteControl = gate.RemoteControl;
                    found.InvoiceNumber = gate.InvoiceNumber;
                    found.InvoiceDate = gate.InvoiceDate;
                    found.PriceMaintenance = gate.PriceMaintenance;
                    found.Active = gate.Active;
                    found.Modified = gate.Modified;
                    found.ModifiedBy = gate.ModifiedBy;

                    entities.SaveChanges();
                }
                else
                    throw new Exception("Geen poort gevonden om te updaten!");
            }
        }

        public void Delete(CustomerGate gate)
        {
            using (var entities = new PoortserviceEntities())
            {
                CustomerGate foundgate = (from g in entities.CustomerGate where g.Id == gate.Id select g).FirstOrDefault();

                if (foundgate != null)
                {
                    foundgate.Active = false;
                    foundgate.Modified = gate.Modified;
                    foundgate.ModifiedBy = gate.ModifiedBy;
                    entities.SaveChanges();

                    List<Intervention> gateInterventions = (from i in entities.Intervention where i.CustomerGate == gate.Id select i).ToList();
                    foreach (Intervention intervention in gateInterventions)
                    {
                        intervention.Active = false;
                        intervention.Modified = gate.Modified;
                        intervention.ModifiedBy = gate.ModifiedBy;
                        entities.SaveChanges();
                    }
                }
                else
                    throw new Exception("Geen poort gevonden om te verwijderen!");
            }
        }
    }
}
