﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Railsystem_services
    {
        public List<RailSystem> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from r in entities.RailSystem
                        where r.Active == true
                        orderby r.Name
                        select r).ToList();
            }
        }

        public ObservableCollection<RailSystem> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<RailSystem>((from r in entities.RailSystem
                                                             where r.Active == true
                                                             orderby r.Name
                                                             select r).ToList());
            }
        }

        public void Delete(RailSystem system)
        {
            system.Active = false;
            Update(system);
        }

        public RailSystem Save(RailSystem system)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from r in entities.RailSystem
                             where r.Name.ToLower() == system.Name.ToLower()
                             select r).FirstOrDefault();

                if (found == null)
                {
                    entities.RailSystem.Add(system);
                    entities.SaveChanges();
                    return system;
                }
                else
                    throw new Exception("Het railsysteem bestaat reeds!");
            }
        }

        public void Update(RailSystem system)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from r in entities.RailSystem
                             where r.Id == system.Id
                             select r).FirstOrDefault();

                if (found != null)
                {
                    found.Name = system.Name;
                    found.Active = system.Active;
                    found.Modified = system.Modified;
                    found.ModifiedBy = system.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Het railsysteem is niet gevonden!");
            }
        }
    }
}
