﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Document_service
    {
        public List<Document> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from d in entities.Document.Include("Language1")
                        where d.Active == true
                        select d).ToList();
            }
        }

        public ObservableCollection<Document> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Document>((from d in entities.Document.Include("Language1")
                                                           where d.Active == true
                                                           select d).ToList());
            }
        }

        public Document Save(Document doc)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from d in entities.Document
                             where d.Url == doc.Url
                             && d.Language == doc.Language
                             select d).FirstOrDefault();

                if (found == null)
                {
                    entities.Document.Add(doc);
                    entities.SaveChanges();
                    return (from d in entities.Document.Include("Language1")
                            where d.Id == doc.Id
                            select d).FirstOrDefault();
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        entities.SaveChanges();
                        return (from d in entities.Document.Include("Language1")
                                where d.Id == found.Id
                                select d).FirstOrDefault();
                    }
                    else
                        throw new Exception("Het document bestaat reeds!");
                }
            }
        }

        public void Update(Document doc)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from d in entities.Document
                             where d.Id == doc.Id
                             select d).FirstOrDefault();

                if (found != null)
                {
                    found.Active = doc.Active;
                    found.Url = doc.Url;
                    found.Language = doc.Language;
                    found.Modified = doc.Modified;
                    found.ModifiedBy = doc.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("Het document bestaat reeds!");
                }
            }
        }

        public void Delete(Document doc)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from d in entities.Document
                             where d.Id == doc.Id
                             select d).FirstOrDefault();
                 if (found != null)
                 {
                     entities.Document.Remove(doc);
                     entities.SaveChanges();
                 }
            }
        }

        public Document GetByLangAndUrl(Document doc)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from d in entities.Document
                        where d.Language == doc.Language
                        && d.Url == doc.Url
                        select d).FirstOrDefault();
            }
        }
    }
}
