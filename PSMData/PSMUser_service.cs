﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class PSMUser_service
    {
        public PSMUser GetUserByNameAndPassword(string username, string password)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from u in entities.PSMUser
                                where u.FirstName.ToLower() == username.ToLower()
                                && u.Password == password
                                && u.Enabled == true
                                select u).FirstOrDefault();
            }
        }

        public ObservableCollection<PSMUser> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<PSMUser>((from u in entities.PSMUser.Include("PSMRole1").Include("Language1")
                                                          where u.Active == true
                                                          select u).ToList());
            }
              
        }

        public PSMUser Save(PSMUser user)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from u in entities.PSMUser
                             where u.FirstName.ToLower() == user.FirstName.ToLower()
                        && u.LastName.ToLower() == user.LastName.ToLower()
                        && u.UserName.ToLower() == user.UserName.ToLower()
                        select u).FirstOrDefault();

                if (found == null)
                {
                    entities.PSMUser.Add(user);
                    entities.SaveChanges();
                    return (from u in entities.PSMUser.Include("PSMRole1").Include("Language1")
                            where u.Id == user.Id
                            select u).FirstOrDefault();
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        found.Password = user.Password;
                        found.PSMRole = user.PSMRole;
                        found.Enabled = user.Enabled;
                        found.LocalWorkDir = user.LocalWorkDir;
                        found.Phone = user.Phone;
                        found.Email = user.Email;
                        found.Modified = user.Modified;
                        found.ModifiedBy = user.ModifiedBy;
                        entities.SaveChanges();
                        return found;
                    }
                    else throw new Exception("Deze gebruiker bestaat reeds!");
                }
            }
        }

        public void Update(PSMUser user)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from u in entities.PSMUser
                             where u.Id == user.Id
                             select u).FirstOrDefault();

                if (found != null)
                {
                    found.UserName = user.UserName;
                    found.FirstName = user.FirstName;
                    found.LastName = user.LastName;
                    found.Active = user.Active;
                    found.Password = user.Password;
                    found.PSMRole = user.PSMRole;
                    found.Enabled = user.Enabled;
                    found.Language = user.Language;
                    found.LocalWorkDir = user.LocalWorkDir;
                    found.Phone = user.Phone;
                    found.Email = user.Email;
                    found.Modified = user.Modified;
                    found.ModifiedBy = user.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De gebruiker is niet gevonden!");
                }
            }
        }

        public void Delete(PSMUser user)
        {
            user.Active = false;
            Update(user);
        }
    }
}
