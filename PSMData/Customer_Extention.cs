﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public partial class Customer
    {
        public override bool Equals(object obj)
        {
            if (obj is Customer)
            {
                Customer toCompare = (Customer)obj;
                if (this.Code != toCompare.Code)
                    return false;
                if (this.Name != toCompare.Name)
                    return false;
                if (this.Address != toCompare.Address)
                    return false;
                if (this.Phone != toCompare.Phone)
                    return false;
                if (this.MobilePhone != toCompare.MobilePhone)
                    return false;
                if (this.Email != toCompare.Email)
                    return false;
                if (this.VatNumber != toCompare.VatNumber)
                    return false;
                if (this.Language != toCompare.Language)
                    return false;
                if (this.City != toCompare.City)
                    return false;
                if (this.LegalCapacity != toCompare.LegalCapacity)
                    return false;
                return true;
            }
            else
                return false;
        }
    }
}
