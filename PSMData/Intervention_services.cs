﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Intervention_services
    {
        public ObservableCollection<Intervention> GetByCustomerGate(int customergate_Id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Intervention>((from i in entities.Intervention.Include("InterventionType1")
                                                               where i.Active == true
                                                               && i.CustomerGate == customergate_Id
                                                               orderby i.PlannedDate descending
                                                               select i).ToList());
            }
        }

        public Intervention Save(Intervention intervention)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from i in entities.Intervention
                             where i.InterventionType == intervention.InterventionType
                             && i.PlannedDate == intervention.PlannedDate
                             && i.CustomerGate == intervention.CustomerGate
                             && i.Active == true
                             select i).FirstOrDefault();

                if (found == null)
                {
                    entities.Intervention.Add(intervention);
                    entities.SaveChanges();

                    return (from i in entities.Intervention.Include("InterventionType1")
                            where i.Id == intervention.Id
                            select i).FirstOrDefault();
                }
                else
                    throw new Exception("De interventie is reeds geregistreerd voor deze datum!");
            }
        }

        public Intervention Update(Intervention intervention)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from i in entities.Intervention
                             where i.Id == intervention.Id
                             select i).FirstOrDefault();

                if (found != null)
                {
                    found.InterventionType = intervention.InterventionType;
                    found.Description = intervention.Description;
                    found.ExecutionDate = intervention.ExecutionDate;
                    found.PlannedDate = intervention.PlannedDate;
                    found.Remarks = intervention.Remarks;
                    found.WaitForFeedBack = intervention.WaitForFeedBack;
                    found.Active = intervention.Active;
                    found.Modified = intervention.Modified;
                    found.ModifiedBy = intervention.ModifiedBy;
                    entities.SaveChanges();

                    return intervention;
                }
                else
                    throw new Exception("Geen overeenkomstige interventie gevonden!");
            }
        }

        public void Delete(Intervention intervention)
        {
            intervention.Active = false;
            Update(intervention);
        }

        public ObservableCollection<Intervention> GetByPlannedDate(DateTime fromdate, DateTime tilldate)
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<Intervention>((from i in entities.Intervention.Include("CustomerGate1").Include("CustomerGate1.GateModel1").Include("CustomerGate1.Customer1").Include("CustomerGate1.Customer1.City1")
                                                               where i.Active == true
                                                               && i.PlannedDate <= tilldate
                                                               && i.PlannedDate >= fromdate
                                                               && i.ExecutionDate == null
                                                               orderby i.PlannedDate descending
                                                               select i).ToList());
            }
        }

        public List<Intervention> GetByCustomerGateAndDate(int customergateID, DateTime? plannedDate)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from i in entities.Intervention
                        where i.CustomerGate == customergateID
                        && i.PlannedDate > (DateTime)plannedDate
                        && i.ExecutionDate == null
                        && i.Active == true
                        select i).ToList();
            }
        }

        public List<Intervention> GetNextByCustomerGate(int customergateID)
        {
            using (var entities = new PoortserviceEntities())
            {               
                return (from i in entities.Intervention
                        where i.CustomerGate == customergateID
                        && i.PlannedDate > DateTime.Today
                        && i.ExecutionDate == null
                        && i.Active == true
                        select i).ToList();
            }
        }

        public Intervention GetGetByID(int id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from i in entities.Intervention.Include("CustomerGate1").Include("CustomerGate1.GateModel1").Include("CustomerGate1.Customer1").Include("CustomerGate1.Customer1.City1")
                        where i.Id == id
                        select i).FirstOrDefault();
            }
        }
    }
}
