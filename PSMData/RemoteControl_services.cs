﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class RemoteControl_services
    {
        public List<RemoteControl> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from r in entities.RemoteControl
                        where r.Active == true
                        orderby r.Name
                        select r).ToList();
            }
        }

        public ObservableCollection<RemoteControl> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<RemoteControl>((from r in entities.RemoteControl
                                                                where r.Active == true
                                                                orderby r.Name
                                                                select r).ToList());
            }
        }

        public RemoteControl Save(RemoteControl control)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from r in entities.RemoteControl
                             where r.Name.ToLower() == control.Name.ToLower()
                             select r).FirstOrDefault();

                if (found == null)
                {
                    entities.RemoteControl.Add(control);
                    entities.SaveChanges();

                    return control;
                }
                else
                {
                    throw new Exception("De afstandsbediening bestaat reeds!");
                }
            }
        }

        public void Update(RemoteControl control)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from r in entities.RemoteControl
                             where r.Id == control.Id
                             select r).FirstOrDefault();

                if (found != null)
                {
                    found.Name = control.Name;
                    found.Active = control.Active;
                    found.Modified = control.Modified;
                    found.ModifiedBy = control.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De afstandsbediening is niet gevonden!");
                }
            }
        }

        public void Delete(RemoteControl control)
        {
            control.Active = false;
            Update(control);
        }
    }
}
