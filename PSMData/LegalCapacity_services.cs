﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace PSMData
{
    public class LegalCapacity_services
    {
        public List<LegalCapacity> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from l in entities.LegalCapacity
                        where l.Active == true
                        orderby l.Name
                        select l).ToList();
            }
        }

        public System.Collections.ObjectModel.ObservableCollection<LegalCapacity> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<LegalCapacity>((from l in entities.LegalCapacity
                                                                where l.Active == true
                                                                orderby l.Name
                                                                select l).ToList());
            }
        }


        public void Delete(LegalCapacity legal)
        {
            legal.Active = false;
            Update(legal);
        }

        public LegalCapacity Save(LegalCapacity legal)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from l in entities.LegalCapacity
                             where l.Name.ToLower() == legal.Name.ToLower()
                        select l).FirstOrDefault();

                if (found == null)
                {
                    entities.LegalCapacity.Add(legal);
                    entities.SaveChanges();

                    return legal;
                }
                else
                    throw new Exception("De hoedanigheid bestaat reeds!");
            }
        }

        public void Update(LegalCapacity legal)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from l in entities.LegalCapacity
                             where l.Id == legal.Id
                             select l).FirstOrDefault();

                if (found != null)
                {
                    found.Name = legal.Name;
                    found.Active = legal.Active;
                    found.Modified = legal.Modified;
                    found.ModifiedBy = legal.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                    throw new Exception("De hoedanigheid is niet gevonden!");
            }
        }
    }
}
