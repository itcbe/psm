﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSMData;

namespace PSMData
{
    public class CustomerSearchTerms
    {
        public string Code { get; set; }
        public string Address { get; set; }
        public string GateColor { get; set; }
        public DateTime? InstalationAfter { get; set; }
        public string Name { get; set; }
        public City City { get; set; }
        public GateModel GateModel { get; set; }
        public string GateNumber { get; set; }
    }
}
