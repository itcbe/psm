﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class CustomerGate_SafetyMeasure_Services
    {
        public List<CustomerGate_SafetyMeasure> GetByCustomerGate(int customergate_id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from cg in entities.CustomerGate_SafetyMeasure
                        where cg.Active == true
                        && cg.CustomerGate == customergate_id
                        select cg).ToList();
            }
        }

        public void SaveList(List<CustomerGate_SafetyMeasure> safetymaesures)
        {
            using (var entities = new PoortserviceEntities())
            {
                foreach (CustomerGate_SafetyMeasure item in safetymaesures)
                {
                    try
                    {
                        item.Active = true;
                        item.Created = DateTime.Now;
                        item.CreatedBy = item.ModifiedBy;
                        item.Modified = item.Created;
                        Save(item);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
        }

        private void DeleteList(CustomerGate_SafetyMeasure item)
        {
            throw new NotImplementedException();
        }

        private void Save(CustomerGate_SafetyMeasure item)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from cg in entities.CustomerGate_SafetyMeasure
                             where cg.SafetyMeasure == item.SafetyMeasure
                             && cg.CustomerGate == item.CustomerGate
                             select cg).FirstOrDefault();

                if (found == null)
                {
                    entities.CustomerGate_SafetyMeasure.Add(item);
                    entities.SaveChanges();
                }
                else
                {
                    found.ModifiedBy = item.ModifiedBy;
                    found.Modified = DateTime.Now;
                    found.Active = true;
                    entities.SaveChanges();
                }
            }
        }

        public void DeleteList(List<CustomerGate_SafetyMeasure> InactiveSafetyMaesures)
        {
            foreach (CustomerGate_SafetyMeasure item in InactiveSafetyMaesures)
            {
                CustomerGate_SafetyMeasure found = GetByCustomergateIDAndSafetymaesureID(item);
                if (found != null)
                {
                    found.Active = false;
                    found.Modified = DateTime.Now;
                    found.ModifiedBy = item.ModifiedBy;
                    Update(found);
                }
            }
        }

        private CustomerGate_SafetyMeasure GetByCustomergateIDAndSafetymaesureID(CustomerGate_SafetyMeasure item)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from cm in entities.CustomerGate_SafetyMeasure
                        where cm.CustomerGate == item.CustomerGate
                        && cm.SafetyMeasure == item.SafetyMeasure
                        select cm).FirstOrDefault();
            }
        }

        private void Update(CustomerGate_SafetyMeasure item)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from cm in entities.CustomerGate_SafetyMeasure
                             where cm.Id == item.Id
                             select cm).FirstOrDefault();

                if (found != null)
                {
                    found.Active = item.Active;
                    found.ModifiedBy = item.ModifiedBy;
                    found.Modified = item.Modified;

                    entities.SaveChanges();
                }
            }
        }
    }
}
