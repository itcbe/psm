//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSMData
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerGate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CustomerGate()
        {
            this.CustomerGate_SafetyMeasure = new HashSet<CustomerGate_SafetyMeasure>();
            this.CustomerGate_ServiceInstallation = new HashSet<CustomerGate_ServiceInstallation>();
            this.Intervention = new HashSet<Intervention>();
        }
    
        public int Id { get; set; }
        public string PGID { get; set; }
        public int Customer { get; set; }
        public int GateModel { get; set; }
        public int Quantity { get; set; }
        public string GateProductionNumber { get; set; }
        public string GateColor { get; set; }
        public Nullable<int> EngineType { get; set; }
        public string EngineProductionNumber { get; set; }
        public Nullable<bool> MaintenanceContract { get; set; }
        public Nullable<int> MaintenanceFrequency { get; set; }
        public string Measurement { get; set; }
        public Nullable<int> GateDecoration { get; set; }
        public Nullable<int> RailSystem { get; set; }
        public Nullable<System.DateTime> InstallationDate { get; set; }
        public Nullable<int> Voltage { get; set; }
        public Nullable<int> GateDriver { get; set; }
        public Nullable<int> RemoteControl { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public Nullable<decimal> PriceMaintenance { get; set; }
        public Nullable<bool> Active { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
    
        public virtual Customer Customer1 { get; set; }
        public virtual EngineType EngineType1 { get; set; }
        public virtual GateDecoration GateDecoration1 { get; set; }
        public virtual GateDriver GateDriver1 { get; set; }
        public virtual GateModel GateModel1 { get; set; }
        public virtual MaintenanceFrequency MaintenanceFrequency1 { get; set; }
        public virtual RailSystem RailSystem1 { get; set; }
        public virtual RemoteControl RemoteControl1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerGate_SafetyMeasure> CustomerGate_SafetyMeasure { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerGate_ServiceInstallation> CustomerGate_ServiceInstallation { get; set; }
        public virtual Voltage Voltage1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Intervention> Intervention { get; set; }
    }
}
