﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class CustomerGate_ServiceInstallation_services
    {
        public List<CustomerGate_ServiceInstallation> GetByCustomerGate(int customergate_id)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from cg in entities.CustomerGate_ServiceInstallation
                        where cg.Active == true
                        && cg.CustomerGate == customergate_id
                        select cg).ToList();
            }
        }

        public void Save(CustomerGate_ServiceInstallation service)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from cs in entities.CustomerGate_ServiceInstallation
                             where cs.CustomerGate == service.CustomerGate
                             && cs.ServiceInstallation == service.ServiceInstallation
                             select cs).FirstOrDefault();

                if (found == null)
                {
                    entities.CustomerGate_ServiceInstallation.Add(service);
                    entities.SaveChanges();
                }
                else
                {
                    found.ModifiedBy = service.ModifiedBy;
                    found.Modified = DateTime.Now;
                    found.Active = true;
                    entities.SaveChanges();
                }
            }
        }

        public void Update(CustomerGate_ServiceInstallation service)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from cs in entities.CustomerGate_ServiceInstallation
                             where cs.CustomerGate == service.CustomerGate
                             && cs.ServiceInstallation == service.ServiceInstallation
                             select cs).FirstOrDefault();

                if (found != null)
                {
                    found.Active = service.Active;
                    found.ModifiedBy = service.ModifiedBy;
                    found.Modified = service.Modified;
                    //entities.CustomerGate_ServiceInstallation.Add(service);
                    entities.SaveChanges();
                }
            }
        }

        public void SaveList(List<CustomerGate_ServiceInstallation> activeServices)
        {
            using (var entities = new PoortserviceEntities())
            {
                foreach (CustomerGate_ServiceInstallation item in activeServices)
                {
                    try
                    {
                        item.Active = true;
                        item.Created = DateTime.Now;
                        item.CreatedBy = item.ModifiedBy;
                        item.Modified = item.Created;
                        Save(item);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
        }

        public void DeleteList(List<CustomerGate_ServiceInstallation> inActiveServices)
        {
            foreach (CustomerGate_ServiceInstallation item in inActiveServices)
            {
                CustomerGate_ServiceInstallation found = GetByCustomergateIDAndServiceInstallationID(item);
                if (found != null)
                {
                    found.Active = false;
                    found.Modified = DateTime.Now;
                    found.ModifiedBy = item.ModifiedBy;
                    Update(found);
                }
            }
        }

        private CustomerGate_ServiceInstallation GetByCustomergateIDAndServiceInstallationID(CustomerGate_ServiceInstallation item)
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from si in entities.CustomerGate_ServiceInstallation
                        where si.CustomerGate == item.CustomerGate
                        && si.ServiceInstallation == item.ServiceInstallation
                        select si).FirstOrDefault();
            }
        }

    }
}
