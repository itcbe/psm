﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Gatemodel_services
    {
        public List<GateModel> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from gm in entities.GateModel.Include("GateBrand1")
                        where gm.Active == true
                        && gm.Model != ""
                        orderby gm.Model
                        select gm).ToList();
            }
        }

        public ObservableCollection<GateModel> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<GateModel>((from gm in entities.GateModel.Include("GateBrand1")
                        where gm.Active == true
                        && gm.Model != ""
                        orderby gm.Model
                        select gm).ToList());
            }
        }

        public void Delete(GateModel model)
        {
            model.Active = false;
            Update(model);
        }

        public GateModel Save(GateModel model)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from gm in entities.GateModel.Include("GateBrand1")
                             where gm.Model.ToLower() == model.Model.ToLower()
                             && gm.GateBrand == model.GateBrand
                             select gm).FirstOrDefault();

                if (found == null)
                {
                    entities.GateModel.Add(model);
                    entities.SaveChanges();

                    return (from gm in entities.GateModel.Include("GateBrand1")
                            where gm.Id == model.Id
                            select gm).FirstOrDefault();
                }
                else
                    throw new Exception("Het poortmodel staat reeds in de lijst!");
            }
        }

        public void Update(GateModel model)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from gm in entities.GateModel.Include("GateBrand1")
                             where gm.Id == model.Id
                             select gm).FirstOrDefault();

                if (found != null)
                {
                    found.Model = model.Model;
                    found.GateBrand = model.GateBrand;
                    found.Active = model.Active;
                    found.Modified = model.Modified;
                    found.ModifiedBy = model.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Het poortmodel is niet gevonden!");
            }
        }
    }
}
