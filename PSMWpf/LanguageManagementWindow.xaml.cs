﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for LanguageManagementWindow.xaml
    /// </summary>
    public partial class LanguageManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource languageViewSource;
        private ObservableCollection<Language> languages;

        public LanguageManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<Language>((from f in languages
                                                                   where f.Name.ToLower().StartsWith(searchString.ToLower())
                                                                   select f).ToList());

                languageViewSource.Source = gevonden;
            }
            else
                languageViewSource.Source = languages;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (languageDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de taal verwijderen?", "Verwijderen taal", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Language language = (Language)languageDataGrid.SelectedItem;
                    Language_services lService = new Language_services();
                    language.Modified = DateTime.Now;
                    language.ModifiedBy = currentUser.FirstName;
                    lService.Delete(language);
                    ReloadLanguages();
                    MessageBox.Show("Taal verwijderd.", "Verwijderen taal");
                }
            }

        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            languageViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("languageViewSource")));
            ReloadLanguages();
        }

        private void ReloadLanguages()
        {
            Language_services lService = new Language_services();
            languages = lService.GetAllObservable();
            languageViewSource.Source = languages;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (languageDataGrid.SelectedIndex > -1)
            {
                Language language = (Language)languageDataGrid.SelectedItem;
                if (language.Id == 0)
                {
                    int i = 0;
                    foreach (Language item in languages)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    languages.RemoveAt(i);
                    languageViewSource.Source = null;
                    languageViewSource.Source = languages;
                }
            }
        }

        private void NewElement()
        {
            Language newLanguage = new Language();
            newLanguage.Id = 0;
            languages.Add(newLanguage);
            languageViewSource.Source = null;
            languageViewSource.Source = languages;
            languageDataGrid.SelectedIndex = languages.Count - 1;
            languageDataGrid.ScrollIntoView(newLanguage);
        }

        private void SaveData()
        {
            if (languageDataGrid.SelectedIndex > -1)
            {
                Language language = (Language)languageDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateLanguage(language);
                if (valid.IsValid)
                {
                    Language_services lService = new Language_services();
                    if (language.Id == 0)
                    {
                        language.Active = true;
                        language.Created = DateTime.Now;
                        language.CreatedBy = currentUser.FirstName;
                        language.Modified = DateTime.Now;
                        language.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            Language saved = lService.Save(language);
                            ReloadLanguages();
                            MessageBox.Show("Taal opgeslagen.", "Opslaan taal");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        language.Modified = DateTime.Now;
                        language.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            lService.Update(language);
                            ReloadLanguages();
                            MessageBox.Show("Taal opgeslagen.", "Opslaan taal");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
