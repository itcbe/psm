﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for RemotecontrolManagementWindow.xaml
    /// </summary>
    public partial class RemotecontrolManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource remoteControlViewSource;
        private ObservableCollection<RemoteControl> remotecontrols;

        public RemotecontrolManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<RemoteControl>((from f in remotecontrols
                                                                     where f.Name.ToLower().StartsWith(searchString.ToLower())
                                                                     select f).ToList());

                remoteControlViewSource.Source = gevonden;
            }
            else
                remoteControlViewSource.Source = remotecontrols;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (remoteControlDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de afstandsbediening verwijderen?", "Verwijderen afstandsbediening", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    RemoteControl system = (RemoteControl)remoteControlDataGrid.SelectedItem;
                    RemoteControl_services rService = new RemoteControl_services();
                    system.Modified = DateTime.Now;
                    system.ModifiedBy = currentUser.FirstName;
                    rService.Delete(system);
                    ReloadRemoteControls();
                    MessageBox.Show("Afstandsbediening verwijderd.", "Verwijderen afstandsbediening");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            remoteControlViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("remoteControlViewSource")));
            ReloadRemoteControls();
        }

        private void ReloadRemoteControls()
        {
            RemoteControl_services rService = new RemoteControl_services();
            remotecontrols = rService.GetAllObservable();
            remoteControlViewSource.Source = remotecontrols;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (remoteControlDataGrid.SelectedIndex > -1)
            {
                RemoteControl remote = (RemoteControl)remoteControlDataGrid.SelectedItem;
                if (remote.Id == 0)
                {
                    int i = 0;
                    foreach (RemoteControl item in remotecontrols)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    remotecontrols.RemoveAt(i);
                    remoteControlViewSource.Source = null;
                    remoteControlViewSource.Source = remotecontrols;
                }
            }
        }


        private void NewElement()
        {
            RemoteControl newControl = new RemoteControl();
            newControl.Id = 0;
            remotecontrols.Add(newControl);
            remoteControlViewSource.Source = null;
            remoteControlViewSource.Source = remotecontrols;
            remoteControlDataGrid.SelectedIndex = remotecontrols.Count - 1;
            remoteControlDataGrid.ScrollIntoView(newControl);
        }

        private void SaveData()
        {
            if (remoteControlDataGrid.SelectedIndex > -1)
            {
                RemoteControl control = (RemoteControl)remoteControlDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateRemoteControl(control);
                if (valid.IsValid)
                {
                    RemoteControl_services rService = new RemoteControl_services();
                    if (control.Id == 0)
                    {
                        control.Active = true;
                        control.Created = DateTime.Now;
                        control.CreatedBy = currentUser.FirstName;
                        control.Modified = DateTime.Now;
                        control.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            RemoteControl saved = rService.Save(control);
                            ReloadRemoteControls();
                            MessageBox.Show("Afstandsbediening opgeslagen.", "Opslaan Afstandsbediening");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        control.Modified = DateTime.Now;
                        control.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            rService.Update(control);
                            ReloadRemoteControls();
                            MessageBox.Show("Afstandsbediening opgeslagen.", "Opslaan Afstandsbediening");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
