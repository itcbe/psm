﻿using PSMData;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            TryLogin();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TryLogin();
            }
            else
            {
                this.Height = 372;
            }
        }

        private void TryLogin()
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Password;
            PSMUser_service uService = new PSMUser_service();
            PSMUser user = uService.GetUserByNameAndPassword(username, password);
            if (user == null)
            {
                errorMessageLabel.Content = "Gebruikersnaam of paswoord is niet correct! Probeer opnieuw.";
                this.Height = 400;
            }
            else
            {
                if (rememberMeCheckBox.IsChecked == true)
                    SetRememberMe();
                MainWindow MW = new MainWindow(user);
                MW.Show();
                this.Hide();
            }
        }

        private void SetRememberMe()
        {
            try
            {
                IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForAssembly();
                //create a stream writer object to write content in the location
                StreamWriter srWriter = new StreamWriter(new IsolatedStorageFileStream("rememberme", FileMode.Create, isolatedStorage));
                srWriter.WriteLine(UsernameTextBox.Text);
                srWriter.WriteLine(PasswordTextBox.Password);

                srWriter.Flush();
                srWriter.Close();
            }
            catch (System.Security.SecurityException sx)
            {
                MessageBox.Show("Kan de login gegevens niet opslaan!\n" + sx.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kan de login gegevens niet opslaan!\n" + ex.Message);
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForAssembly();
                StreamReader srReader = new StreamReader(new IsolatedStorageFileStream("rememberme", FileMode.OpenOrCreate, storage));

                if (srReader != null)
                {
                    string username = "";
                    string pass = "";
                    int i = 1;
                    while (!srReader.EndOfStream)
                    {
                        if (i == 1)
                            username = srReader.ReadLine();
                        else if (i == 2)
                            pass = srReader.ReadLine();
                        i++;
                    }

                    UsernameTextBox.Text = username;
                    PasswordTextBox.Password = pass;
                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(pass))
                        rememberMeCheckBox.IsChecked = true;
                    else
                        rememberMeCheckBox.IsChecked = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("kan de opgeslagen gebruiker niet laden!\n" + ex.Message);
            }

            UsernameTextBox.Focus();
        }
    }
}
