﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for RailsystemManagementWindow.xaml
    /// </summary>
    public partial class RailsystemManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource railSystemViewSource;
        private ObservableCollection<RailSystem> railsystems;

        public RailsystemManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<RailSystem>((from f in railsystems
                                                                    where f.Name.ToLower().StartsWith(searchString.ToLower())
                                                                    select f).ToList());

                railSystemViewSource.Source = gevonden;
            }
            else
                railSystemViewSource.Source = railsystems;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (railSystemDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het railsysteem verwijderen?", "Verwijderen railsysteem", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    RailSystem system = (RailSystem)railSystemDataGrid.SelectedItem;
                    Railsystem_services rService = new Railsystem_services();
                    system.Modified = DateTime.Now;
                    system.ModifiedBy = currentUser.FirstName;
                    rService.Delete(system);
                    ReloadRailsystems();
                    MessageBox.Show("Railsysteem poortmodel.", "Verwijderen railsysteem");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            railSystemViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("railSystemViewSource")));
            ReloadRailsystems();
        }

        private void ReloadRailsystems()
        {
            Railsystem_services rService = new Railsystem_services();
            railsystems = rService.GetAllObservable();
            railSystemViewSource.Source = railsystems;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (railSystemDataGrid.SelectedIndex > -1)
            {
                RailSystem railsystem = (RailSystem)railSystemDataGrid.SelectedItem;
                if (railsystem.Id == 0)
                {
                    int i = 0;
                    foreach (RailSystem item in railsystems)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    railsystems.RemoveAt(i);
                    railSystemViewSource.Source = null;
                    railSystemViewSource.Source = railsystems;
                }
            }
        }


        private void NewElement()
        {
            RailSystem newSystem = new RailSystem();
            newSystem.Id = 0;
            railsystems.Add(newSystem);
            railSystemViewSource.Source = null;
            railSystemViewSource.Source = railsystems;
            railSystemDataGrid.SelectedIndex = railsystems.Count - 1;
            railSystemDataGrid.ScrollIntoView(newSystem);
        }

        private void SaveData()
        {
            if (railSystemDataGrid.SelectedIndex > -1)
            {
                RailSystem system = (RailSystem)railSystemDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateRailSystem(system);
                if (valid.IsValid)
                {
                    Railsystem_services rService = new Railsystem_services();
                    if (system.Id == 0)
                    {
                        system.Active = true;
                        system.Created = DateTime.Now;
                        system.CreatedBy = currentUser.FirstName;
                        system.Modified = DateTime.Now;
                        system.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            RailSystem saved = rService.Save(system);
                            ReloadRailsystems();
                            MessageBox.Show("Railsysteem opgeslagen.", "Opslaan Railsysteem");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        system.Modified = DateTime.Now;
                        system.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            rService.Update(system);
                            ReloadRailsystems();
                            MessageBox.Show("Railsysteem opgeslagen.", "Opslaan Railsysteem");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
