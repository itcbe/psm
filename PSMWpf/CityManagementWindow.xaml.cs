﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for CityManagementWindow.xaml
    /// </summary>
    public partial class CityManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource cityViewSource;
        private ObservableCollection<City> cities;

        public CityManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<City>((from c in cities
                                                               where c.Zipcode.Contains(searchString)
                                                               || c.Name.ToLower().StartsWith(searchString.ToLower())
                                                               select c).ToList());

                cityViewSource.Source = gevonden;
            }
            else
                cityViewSource.Source = cities;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            if (cityDataGrid.SelectedIndex > -1)
            {
                City city = (City)cityDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateCity(city);
                if (valid.IsValid)
                {
                    City_services vService = new City_services();
                    if (city.Id == 0)
                    {
                        city.Country1 = null;
                        city.Active = true;
                        city.Created = DateTime.Now;
                        city.CreatedBy = currentUser.FirstName;
                        city.Modified = DateTime.Now;
                        city.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            City saved = vService.Save(city);
                            ReloadCities();
                            MessageBox.Show("Gemeente opgeslagen.", "Opslaan Gemeente");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        city.Modified = DateTime.Now;
                        city.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            vService.Update(city);
                            ReloadCities();
                            MessageBox.Show("Gemeente opgeslagen.", "Opslaan Gemeente");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (cityDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de aansluitspanning verwijderen?", "Verwijderen Aansluitspanning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    City city = (City)cityDataGrid.SelectedItem;
                    City_services cService = new City_services();
                    city.Modified = DateTime.Now;
                    city.ModifiedBy = currentUser.FirstName;
                    cService.Delete(city);
                    ReloadCities();
                    MessageBox.Show("Aansluitspanning verwijderd.", "Verwijderen Aansluitspanning");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Country_services coService = new Country_services();
            List<Country> countries = coService.GetAll();
            countryComboBox.ItemsSource = countries;
            countryComboBox.DisplayMemberPath = "Name";

            cityViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("cityViewSource")));
            ReloadCities();
        }

        private void ReloadCities()
        {
            City_services cService = new City_services();
            cities = cService.GetAllObservable();
            cityViewSource.Source = cities;
        }

        private void cityDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            City city = (City)dg.SelectedItem;
            if (city != null)
            {
                try
                {
                    countryComboBox.Text = city.Country1.Name;
                }
                catch (Exception)
                {
                    countryComboBox.SelectedIndex = -1;
                }
            }
        }

        private void countryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            City city = (City)cityDataGrid.SelectedItem;
            if (city != null)
            {
                Country country = (Country)box.SelectedItem;
                if (country != null)
                {
                    city.Country1 = country;
                    city.Country = country.Id;
                }
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (cityDataGrid.SelectedIndex > -1)
            {
                City city = (City)cityDataGrid.SelectedItem;
                if (city.Id == 0)
                {
                    int i = 0;
                    foreach (City item in cities)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    cities.RemoveAt(i);
                    cityViewSource.Source = null;
                    cityViewSource.Source = cities;
                }
            }
        }

        private void NewElement()
        {
            City newCity = new City();
            newCity.Id = 0;
            cities.Add(newCity);
            cityViewSource.Source = null;
            cityViewSource.Source = cities;
            cityDataGrid.SelectedIndex = cities.Count - 1;
            cityDataGrid.ScrollIntoView(newCity);
        }
    }
}
