﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PSMData;
using System.Collections.ObjectModel;
using PSMClassLibrary;
using ITCLibrary;
using System.Globalization;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for DocumentCreationWindow.xaml
    /// </summary>
    public partial class DocumentCreationWindow : Window
    {
        private PSMUser currentUser;
        private Customer currentCustomer;
        private CustomerGate currentGate;
        private Template currentTemplate;
        private System.Windows.Data.CollectionViewSource customerViewSource;
        private System.Windows.Data.CollectionViewSource customerGateViewSource;

        public DocumentCreationWindow(PSMUser user, Customer customer)
        {
            InitializeComponent();
            currentUser = user;
            currentCustomer = customer;
        }

        private void CloseButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SearchCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            OpenSearchWindow();
        }

        private void OpenSearchWindow()
        {
            CustomerSearchWindow SW = new CustomerSearchWindow();
            if (SW.ShowDialog() == true)
            {
                CustomerSearchTerms searchterms = SW.searchTerms;
                FilterCustomers(searchterms);
            }
        }

        private void FilterCustomers(CustomerSearchTerms searchterms)
        {
            Customer_services cService = new Customer_services();
            ObservableCollection<Customer> customers = cService.GetBySearchTerms(searchterms);
            customerViewSource.Source = customers;
            //currentCustomers = customers;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Template_service tService = new Template_service();
            List<Template> templates = tService.GetAll();
            TemplateComboBox.ItemsSource = templates;
            TemplateComboBox.DisplayMemberPath = "TemplateType";

            customerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("customerViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // customerViewSource.Source = [generic data source]
            customerGateViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("customerGateViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // customerGateViewSource.Source = [generic data source]
            if (currentCustomer != null)
            {
                ObservableCollection<Customer> customers = new ObservableCollection<Customer>();
                customers.Add(currentCustomer);
                customerViewSource.Source = customers;
            }

        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            Valid valid = OurValidation.ValidateDocumentCreation(currentCustomer, currentGate, currentTemplate);
            if (valid.IsValid)
            {
                CreateDocument(currentCustomer, currentGate, currentTemplate);
            }
            else
                MessageBox.Show(valid.Message);
        }

        private void customerDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg.SelectedIndex > -1)
            {
                currentCustomer = (Customer)dg.SelectedItem;

                Customer_Gate_services cgService = new Customer_Gate_services();
                ObservableCollection<CustomerGate> gates = cgService.GetByCustomer(currentCustomer.Id);
                customerGateViewSource.Source = gates;
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void customerGateDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg.SelectedIndex > -1)
            {
                currentGate = (CustomerGate)dg.SelectedItem;
            }
        }

        private void TemplateComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (true)
            {
                currentTemplate = (Template)box.SelectedItem;
            }
        }

        private void CreateDocument(Customer customer, CustomerGate gate, PSMData.Template template)
        {
            try
            {
                Language language = new Language();
                Language_services lService = new Language_services();
                if (customer.Language1 != null)
                {
                    language = customer.Language1;
                }
                else
                    language = lService.GetByCode("NL");

                Template_Document_service tdService = new Template_Document_service();
                Template_Document templ = tdService.GetByTemplateAndLanguage(template, language);
                // Word template
                object t = templ.Document1.Url;
                // strings for replace in Worddocument
                string today = (DateTime.Today).ToShortDateString();
                string procuctiondate = "";
                if (gate.InstallationDate != null)
                    procuctiondate = ((DateTime)gate.InstallationDate).ToShortDateString();
                string address = customer.City1.Zipcode + ", " + customer.City1.Name.ToUpper();
                string gatemodel = "";
                try
                {
                    if (gate.GateModel1 != null)
                        gatemodel = gate.GateModel1.Model;
                }
                catch (Exception) { }
                string railsystem = "";
                try
                {
                    if (gate.RailSystem1 != null)
                        railsystem = gate.RailSystem1.Name;
                }
                catch (Exception) { }
                string gatedriver = "";
                try
                {
                    if (gate.GateDriver1 != null)
                        gatedriver = gate.GateDriver1.Name;
                }
                catch (Exception) { }
                string remotecontrol = "";
                try
                {
                    if (gate.RemoteControl1 != null)
                        remotecontrol = gate.RemoteControl1.Name;
                }
                catch (Exception) { }
                string voltage = "";
                try
                {
                    if (gate.Voltage1 != null)
                        voltage = gate.Voltage1.Amount.ToString();
                }
                catch (Exception) { }
                string enginetype = "";
                try
                {
                    if (gate.EngineType1 != null)
                        enginetype = gate.EngineType1.Name;
                }
                catch (Exception) { }
                string legalcapacity = "";
                try
                {
                    if (customer.LegalCapacity1 != null)
                        legalcapacity = customer.LegalCapacity1.Name;
                }
                catch (Exception) { }
                string safetyMeasures = GetSafetyMeasureString(gate.Id);
                string serviceinstallations = GetServiceInstalationsString(gate.Id);

                WordDocument doc = new WordDocument(t, "Staand");
                // customer info
                doc.FindAndReplace("#Vandaag#", today);
                doc.FindAndReplace("#Klantnummer#", customer.Code);
                doc.FindAndReplace("#Naam#", customer.Name);
                doc.FindAndReplace("#StraatNummer#", customer.Address);
                doc.FindAndReplace("#PostcodeGemeente#", address);
                doc.FindAndReplace("#Contractnummer#", customer.Code);
                doc.FindAndReplace("#Telefoon#", customer.Phone);
                doc.FindAndReplace("#GSM#", customer.MobilePhone);
                doc.FindAndReplace("#Legalcapacity#", legalcapacity);
                doc.FindAndReplace("#Gemeente#", customer.City1.Name);
                // gate info
                doc.FindAndReplace("#Fabrikagenummer#", gate.GateProductionNumber);
                doc.FindAndReplace("#GateModel#", gatemodel);
                doc.FindAndReplace("#Railsystem#", railsystem);
                doc.FindAndReplace("#Quantity#", gate.Quantity);
                doc.FindAndReplace("#GateDriver#", gatedriver);
                doc.FindAndReplace("#RemoteControl#", remotecontrol);
                //CultureInfo ci = new CultureInfo("be-BE");
                doc.FindAndReplace("#InterventionPrice#", string.Format("{0:N2}", gate.PriceMaintenance));
                doc.FindAndReplace("#Bouwjaar#", procuctiondate);
                doc.FindAndReplace("#InstallationDate#", procuctiondate);
                doc.FindAndReplace("#SafetyMeasures#", safetyMeasures);
                doc.FindAndReplace("#InstallationServices#", serviceinstallations);

                doc.FindAndReplace("#Dementions#", gate.Measurement);
                doc.FindAndReplace("#Gatecolor#", gate.GateColor);
                doc.FindAndReplace("#Voltage#", voltage);
                doc.FindAndReplace("#EngineType#", enginetype);

                doc.Activate();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private string GetServiceInstalationsString(int id)
        {
            string serviceString = "";
            Service_Installation_services sService = new Service_Installation_services();
            List<CustomerGate_ServiceInstallation> installations = sService.GetByGate(id);
            if (installations.Count > 0)
            {
                foreach (CustomerGate_ServiceInstallation installation in installations)
                {
                    serviceString += installation.ServiceInstallation1.Name + ", ";
                }
            }
            if (serviceString.Length > 2)
                serviceString = serviceString.Substring(0, serviceString.Length - 2);
            return serviceString;
        }

        private string GetSafetyMeasureString(int id)
        {
            string measureString = "";
            SafetyMeasure_services smService = new SafetyMeasure_services();
            List<CustomerGate_SafetyMeasure> measures = smService.GetByGate(id);
            if (measures.Count > 0)
            {
                foreach (CustomerGate_SafetyMeasure measure in measures)
                {
                    measureString += measure.SafetyMeasure1.Name + ", ";
                }
            }
            if (measureString.Length > 2)
                measureString = measureString.Substring(0, measureString.Length - 2);
            return measureString;
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                OpenSearchWindow();
            }
            if (e.Key == Key.F4)
            {
                this.Close();
            }
        }
    }
}
