﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for ServiceInstallationManagementWindow.xaml
    /// </summary>
    public partial class ServiceInstallationManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource serviceInstallationViewSource;
        private ObservableCollection<ServiceInstallation> serviceInstallations;

        public ServiceInstallationManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<ServiceInstallation>((from s in serviceInstallations
                                                                              where s.Name.ToLower().StartsWith(searchString.ToLower())
                                                                              select s).ToList());

                serviceInstallationViewSource.Source = gevonden;
            }
            else
                serviceInstallationViewSource.Source = serviceInstallations;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (serviceInstallationDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de bedieningsinstallatie verwijderen?", "Verwijderen Bedieningsinstallatie", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    ServiceInstallation installation = (ServiceInstallation)serviceInstallationDataGrid.SelectedItem;
                    Service_Installation_services sService = new Service_Installation_services();
                    installation.Modified = DateTime.Now;
                    installation.ModifiedBy = currentUser.FirstName;
                    sService.Delete(installation);
                    ReloadServiceInstallations();
                    MessageBox.Show("Bedieningsinstallatie verwijderd.", "Verwijderen Bedieningsinstallatie");
                }
            }

        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            serviceInstallationViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("serviceInstallationViewSource")));
            ReloadServiceInstallations();
        }

        private void ReloadServiceInstallations()
        {
            Service_Installation_services sService = new Service_Installation_services();
            serviceInstallations = sService.GetAllObservable();
            serviceInstallationViewSource.Source = serviceInstallations;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (serviceInstallationDataGrid.SelectedIndex > -1)
            {
                ServiceInstallation service = (ServiceInstallation)serviceInstallationDataGrid.SelectedItem;
                if (service.Id == 0)
                {
                    int i = 0;
                    foreach (ServiceInstallation item in serviceInstallations)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    serviceInstallations.RemoveAt(i);
                    serviceInstallationViewSource.Source = null;
                    serviceInstallationViewSource.Source = serviceInstallations;
                }
            }
        }


        private void NewElement()
        {
            ServiceInstallation newInstallation = new ServiceInstallation();
            newInstallation.Id = 0;
            serviceInstallations.Add(newInstallation);
            serviceInstallationViewSource.Source = null;
            serviceInstallationViewSource.Source = serviceInstallations;
            serviceInstallationDataGrid.SelectedIndex = serviceInstallations.Count - 1;
            serviceInstallationDataGrid.ScrollIntoView(newInstallation);
        }

        private void SaveData()
        {
            if (serviceInstallationDataGrid.SelectedIndex > -1)
            {
                ServiceInstallation installation = (ServiceInstallation)serviceInstallationDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateServiceInstallation(installation);
                if (valid.IsValid)
                {
                    Service_Installation_services sService = new Service_Installation_services();
                    if (installation.Id == 0)
                    {
                        installation.Active = true;
                        installation.Created = DateTime.Now;
                        installation.CreatedBy = currentUser.FirstName;
                        installation.Modified = DateTime.Now;
                        installation.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            ServiceInstallation saved = sService.Save(installation);
                            ReloadServiceInstallations();
                            MessageBox.Show("Bedieningsinstallatie opgeslagen.", "Opslaan Bedieningsinstallatie");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        installation.Modified = DateTime.Now;
                        installation.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            sService.Update(installation);
                            ReloadServiceInstallations();
                            MessageBox.Show("Bedieningsinstallatie opgeslagen.", "Opslaan Bedieningsinstallatie");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
