﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for GateDriverManagementWindow.xaml
    /// </summary>
    public partial class GateDriverManagementWindow : Window
    {
        private PSMUser currentUser;
        private ObservableCollection<GateDriver> gatedrivers;
        private System.Windows.Data.CollectionViewSource gateDriverViewSource;

        public GateDriverManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (gateDriverDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het Sturing verwijderen?", "Verwijderen Sturing", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    GateDriver driver = (GateDriver)gateDriverDataGrid.SelectedItem;
                    GateDriver_services dService = new GateDriver_services();
                    driver.Modified = DateTime.Now;
                    driver.ModifiedBy = currentUser.FirstName;
                    dService.Delete(driver);
                    ReloadGateDrivers();
                    MessageBox.Show("Sturing verwijderd.", "Verwijderen Sturing");
                }
            }
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<GateDriver>(from d in gatedrivers
                                                                    where d.Name.ToLower().StartsWith(searchString.ToLower())
                                                                    orderby d.Name
                                                                    select d).ToList();

                gateDriverViewSource.Source = gevonden;
            }
            else
                gateDriverViewSource.Source = gatedrivers;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            gateDriverViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("gateDriverViewSource")));
            ReloadGateDrivers();
        }

        private void ReloadGateDrivers()
        {
            GateDriver_services dService = new GateDriver_services();
            gatedrivers = dService.GetAllObservable();
            gateDriverViewSource.Source = gatedrivers;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (gateDriverDataGrid.SelectedIndex > -1)
            {
                GateDriver gatedriver = (GateDriver)gateDriverDataGrid.SelectedItem;
                if (gatedriver.Id == 0)
                {
                    int i = 0;
                    foreach (GateDriver item in gatedrivers)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    gatedrivers.RemoveAt(i);
                    gateDriverViewSource.Source = null;
                    gateDriverViewSource.Source = gatedrivers;
                }
            }
        }


        private void NewElement()
        {
            GateDriver newDriver = new GateDriver();
            newDriver.Id = 0;
            gatedrivers.Add(newDriver);
            gateDriverViewSource.Source = null;
            gateDriverViewSource.Source = gatedrivers;
            gateDriverDataGrid.SelectedIndex = gatedrivers.Count - 1;
            gateDriverDataGrid.ScrollIntoView(newDriver);
        }

        private void SaveData()
        {
            if (gateDriverDataGrid.SelectedIndex > -1)
            {
                GateDriver driver = (GateDriver)gateDriverDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateGateDriver(driver);
                if (valid.IsValid)
                {
                    GateDriver_services dService = new GateDriver_services();
                    if (driver.Id == 0)
                    {
                        driver.Active = true;
                        driver.Created = DateTime.Now;
                        driver.CreatedBy = currentUser.FirstName;
                        driver.Modified = DateTime.Now;
                        driver.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            GateDriver saved = dService.Save(driver);
                            ReloadGateDrivers();
                            MessageBox.Show("Sturing opgeslagen.", "Opslaan Sturing");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        driver.Modified = DateTime.Now;
                        driver.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            dService.Update(driver);
                            ReloadGateDrivers();
                            MessageBox.Show("Sturing opgeslagen.", "Opslaan Sturing");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
