﻿using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for MaintenanceOverviewWindow.xaml
    /// </summary>
    public partial class MaintenanceOverviewWindow : Window
    {
        private System.Windows.Data.CollectionViewSource interventionViewSource;
        private PSMUser currentUser;
        private bool IsInit = true;
        private ObservableCollection<Intervention> interventions;

        public MaintenanceOverviewWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            DateTime vandaag = DateTime.Today;
            DateTime firstdate = new DateTime(2003, 1, 1);
            tilDateDatePicker.SelectedDate = vandaag;
            fromDateDatePicker.SelectedDate = firstdate;

            interventionViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("interventionViewSource")));
            LoadInterventions();
            IsInit = false;
        }

        private void LoadInterventions()
        {
            if (tilDateDatePicker.SelectedDate != null && fromDateDatePicker.SelectedDate != null)
            {
                DateTime fromdate = (DateTime)fromDateDatePicker.SelectedDate;
                DateTime tilldate = (DateTime)tilDateDatePicker.SelectedDate;
                Intervention_services iService = new Intervention_services();
                interventions = iService.GetByPlannedDate(fromdate, tilldate);
                interventionViewSource.Source = interventions;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                string csv = "Geplande datum;Klantocde;Klant;Adres;Postcode;Gemeente;Poortmodel\n";
                //csv = String.Join(";", interventions.ToArray());
                foreach (Intervention item in interventions)
                {
                    string gatemodel = "";
                    try
                    {
                        if (item.CustomerGate1.GateModel != 0)
                        {
                            gatemodel = item.CustomerGate1.GateModel1.Model;
                        }
                    }
                    catch (Exception) { }
                    string address = "";
                    try
                    {
                        address = item.CustomerGate1.Customer1.Address.Replace("\n", " :").Replace("\r\n", " :").Replace("\r", " :").Replace("\t", " ");
                    }
                    catch (Exception) { }

                    csv += ((DateTime)item.PlannedDate).ToShortDateString()
                        + ";" + item.CustomerGate1.Customer1.Code
                        + ";" + item.CustomerGate1.Customer1.Name + ";"
                        + address + ";"
                        + item.CustomerGate1.Customer1.City1.Zipcode + ";"
                        + item.CustomerGate1.Customer1.City1.Name + ";"
                        + gatemodel + "\n";
                }
                DateTime date = (DateTime)tilDateDatePicker.SelectedDate;
                var strDate = "";
                if (date != null)
                    strDate = date.Day + "-" + date.Month + "-" + date.Year;
                else
                    strDate = DateTime.Today.Day + "-" + DateTime.Today.Month + "-" + DateTime.Today.Year;

                string path = @"C:\PSM Data\" + "InterventieLijst_" + strDate + ".csv";
                int version = 0;
                bool IsPathValid = false;
                while (!IsPathValid)
                {
                    try
                    {
                        using (var sw = new StreamWriter(path, false, System.Text.Encoding.UTF8))
                        {
                            IsPathValid = true;
                            sw.Write(csv);
                            sw.Close();
                        }
                    }
                    catch (IOException)
                    {
                        version++;
                        path = @"C:\PSM Data\" + "InterventieLijst_" + strDate + "(" + version + ").csv";
                    }
                }
                try
                {
                    System.Diagnostics.Process.Start(path);
                }
                catch (Exception)
                {
                    MessageBox.Show("Fout bij het openen van Excelbestand!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fout bij het maken van de excellijst!\n" + ex.Message);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            //if (executionDateDatePicker.SelectedDate != null)
            //{
            //    Intervention intervention = (Intervention)interventionDataGrid.SelectedItem;
            //    if (intervention != null)
            //    {
            //        intervention.ExecutionDate = (DateTime)executionDateDatePicker.SelectedDate;
            //        intervention.Modified = DateTime.Now;
            //        intervention.ModifiedBy = currentUser.FirstName;
            //        Intervention_services iService = new Intervention_services();
            //        iService.Update(intervention);
            //        LoadInterventions();
            //    }
            //}
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                //if (SaveButton.IsEnabled == true)
                //    SaveData();
            }
            if (e.Key == Key.P && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                CreateCSVFile();
            }
            if (e.Key == Key.F4)
            {
                this.Close();
            }
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsInit)
            {
                LoadInterventions();
            }
        }
    }
}
