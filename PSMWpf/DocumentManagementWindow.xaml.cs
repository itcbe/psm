﻿using Microsoft.Win32;
using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for DocumentManagementWindow.xaml
    /// </summary>
    public partial class DocumentManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource documentViewSource;
        private ObservableCollection<Document> documents;

        public DocumentManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<Document>(from m in documents
                                                                  where m.Url.ToLower().Contains(searchString.ToLower())
                                                                  select m).ToList();

                documentViewSource.Source = gevonden;
            }
            else
                documentViewSource.Source = documents;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DialogResult = true;
            }
            catch (System.InvalidOperationException)
            {
                this.Close();
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            if (documentDataGrid.SelectedIndex > -1)
            {
                Document doc = (Document)documentDataGrid.SelectedItem;
                Language lang = new Language();
                if (languageComboBox.SelectedIndex > -1)
                {
                    lang = (Language)languageComboBox.SelectedItem;
                }
                if (doc.Language1 == null || (lang.Id != 0 && doc.Language != lang.Id))
                {
                    doc.Language = lang.Id;
                    doc.Language1 = lang;
                }

                Valid valid = OurValidation.ValidateDocument(doc);
                if (valid.IsValid)
                {
                    Document_service dService = new Document_service();
                    if (doc.Id == 0)
                    {
                        doc.Language1 = null;
                        doc.Active = true;
                        doc.Created = DateTime.Now;
                        doc.CreatedBy = currentUser.FirstName;
                        doc.Modified = DateTime.Now;
                        doc.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            Document saved = dService.Save(doc);
                            ReloadDocuments();
                            MessageBox.Show("Document opgeslagen.", "Opslaan Poortmodel");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        doc.Modified = DateTime.Now;
                        doc.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            dService.Update(doc);
                            ReloadDocuments();
                            MessageBox.Show("Document opgeslagen.", "Opslaan Poortmodel");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (documentDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het document verwijderen?", "Verwijderen poortmodel", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Document doc = (Document)documentDataGrid.SelectedItem;
                    Document_service dService = new Document_service();
                    doc.Modified = DateTime.Now;
                    doc.ModifiedBy = currentUser.FirstName;
                    dService.Delete(doc);
                    ReloadDocuments();
                    MessageBox.Show("Poortmodel verwijderd.", "Verwijderen poortmodel");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Language_services lService = new Language_services();
            List<Language> languages = lService.GetAll();
            languageComboBox.ItemsSource = languages;
            languageComboBox.DisplayMemberPath = "Name";

            documentViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("documentViewSource")));
            ReloadDocuments();
        }

        private void ReloadDocuments()
        {
            Document_service dService = new Document_service();
            documents = dService.GetAllObservable();
            documentViewSource.Source = documents;
        }

        private void languageComboBox_DropDownOpened(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Language lang = (Language)box.SelectedItem;
                Document doc = (Document)documentDataGrid.SelectedItem;
                if (doc != null)
                {
                    doc.Language = lang.Id;
                    doc.Language1 = lang;
                }
            }
        }

        private void documentDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg.SelectedIndex > -1)
            {
                Document doc = (Document)dg.SelectedItem;
                try
                {
                    languageComboBox.Text = doc.Language1.Name;
                }
                catch (Exception)
                {
                    languageComboBox.SelectedIndex = -1;
                }
            }
        }

        private void OpenUrlButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog OD = new OpenFileDialog();
            OD.DefaultExt = "doc|docx|dot|dotx";
            OD.InitialDirectory = @"C:\PSM Data\Templates\";
            if (OD.ShowDialog() == true)
            {
                int aantal = (from d in documents where d.Url == OD.FileName select d).Count();
                if (aantal < 1)
                {
                    urlTextBox.Text = OD.FileName;
                    Document doc = (Document)documentDataGrid.SelectedItem;
                    doc.Url = OD.FileName;
                }
                else
                    MessageBox.Show("Er is reeds een document met de zelfde Url!\nGelieve een andere Url te kiezen.");
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }

        }

        private void RemoveEmptyElement()
        {
            if (documentDataGrid.SelectedIndex > -1)
            {
                Document document = (Document)documentDataGrid.SelectedItem;
                if (document.Id == 0)
                {
                    int i = 0;
                    foreach (Document item in documents)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    documents.RemoveAt(i);
                    documentViewSource.Source = null;
                    documentViewSource.Source = documents;
                }
            }
        }

        private void NewElement()
        {
            Document newDoc = new Document();
            newDoc.Id = 0;
            documents.Add(newDoc);
            documentViewSource.Source = null;
            documentViewSource.Source = documents;
            documentDataGrid.SelectedIndex = documents.Count - 1;
            documentDataGrid.ScrollIntoView(newDoc);
        }
    }
}
