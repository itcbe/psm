﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for GateModelManagementWindow.xaml
    /// </summary>
    public partial class GateModelManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource gateModelViewSource;
        private ObservableCollection<GateModel> gatemodels;

        public GateModelManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<GateModel>(from m in gatemodels
                                                                    where m.Model.ToLower().StartsWith(searchString.ToLower())
                                                                   orderby m.Model
                                                                    select m).ToList();

                gateModelViewSource.Source = gevonden;
            }
            else
                gateModelViewSource.Source = gatemodels;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (gateModelDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het poortmodel verwijderen?", "Verwijderen poortmodel", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    GateModel model = (GateModel)gateModelDataGrid.SelectedItem;
                    Gatemodel_services mService = new Gatemodel_services();
                    model.Modified = DateTime.Now;
                    model.ModifiedBy = currentUser.FirstName;
                    mService.Delete(model);
                    ReloadGateModels();
                    MessageBox.Show("Poortmodel verwijderd.", "Verwijderen poortmodel");
                }
            }
        }

        private void ReloadGateModels()
        {
            Gatemodel_services mService = new Gatemodel_services();
            gatemodels = mService.GetAllObservable();
            gateModelViewSource.Source = gatemodels;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            GateBrand_services bService = new GateBrand_services();
            List<GateBrand> brands = bService.GetAll();
            gateBrandComboBox.ItemsSource = brands;
            gateBrandComboBox.DisplayMemberPath = "Brand";

            gateModelViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("gateModelViewSource")));
            ReloadGateModels();
        }

        private void gateBrandComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboBox box = (ComboBox)sender;
            if (gateModelDataGrid.SelectedIndex > -1)
            {
                GateBrand brand = (GateBrand)gateBrandComboBox.SelectedItem;
                if (brand != null)
                {
                    GateModel model = (GateModel)gateModelDataGrid.SelectedItem;
                    model.GateBrand = brand.Id;
                    model.GateBrand1 = brand;
                }
            }
        }

        private void gateModelDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            GateModel model = (GateModel)dg.SelectedItem;
            if (model != null)
            {
                try
                {
                    gateBrandComboBox.Text = model.GateBrand1.Brand;
                }
                catch (Exception)
                {
                    gateBrandComboBox.SelectedIndex = -1;
                }
            }
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (gateModelDataGrid.SelectedIndex > -1)
            {
                GateModel gatemodel = (GateModel)gateModelDataGrid.SelectedItem;
                if (gatemodel.Id == 0)
                {
                    int i = 0;
                    foreach (GateModel item in gatemodels)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    gatemodels.RemoveAt(i);
                    gateModelViewSource.Source = null;
                    gateModelViewSource.Source = gatemodels;
                }
            }
        }


        private void NewElement()
        {
            GateModel newModel = new GateModel();
            newModel.Id = 0;
            gatemodels.Add(newModel);
            gateModelViewSource.Source = null;
            gateModelViewSource.Source = gatemodels;
            gateModelDataGrid.SelectedIndex = gatemodels.Count - 1;
            gateModelDataGrid.ScrollIntoView(newModel);
        }

        private void SaveData()
        {
            if (gateModelDataGrid.SelectedIndex > -1)
            {
                GateModel model = (GateModel)gateModelDataGrid.SelectedItem;

                GateBrand brand = new GateBrand();
                if (gateBrandComboBox.SelectedIndex > -1)
                {
                    brand = (GateBrand)gateBrandComboBox.SelectedItem;
                }
                if (model.GateBrand1 == null || (brand.Id != 0 && model.GateBrand != brand.Id))
                {
                    model.GateBrand = brand.Id;
                    model.GateBrand1 = brand;
                }

                Valid valid = OurValidation.ValidateGateModel(model);
                if (valid.IsValid)
                {
                    Gatemodel_services mService = new Gatemodel_services();
                    if (model.Id == 0)
                    {
                        model.GateBrand1 = null;
                        model.Active = true;
                        model.Created = DateTime.Now;
                        model.CreatedBy = currentUser.FirstName;
                        model.Modified = DateTime.Now;
                        model.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            GateModel saved = mService.Save(model);
                            ReloadGateModels();
                            MessageBox.Show("Poortmodel opgeslagen.", "Opslaan Poortmodel");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        model.Modified = DateTime.Now;
                        model.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            mService.Update(model);
                            ReloadGateModels();
                            MessageBox.Show("Poortmodel opgeslagen.", "Opslaan Poortmodel");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
