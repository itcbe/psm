﻿//using PSMClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PSMData;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for CustomerSearchWindow.xaml
    /// </summary>
    public partial class CustomerSearchWindow : Window
    {
        public CustomerSearchTerms searchTerms = new CustomerSearchTerms();

        public CustomerSearchWindow()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void ClearForm()
        {
            codeTextBox.Text = "";
            nameTextBox.Text = "";
            addressTextBox.Text = "";
            zipcodeComboBox.SelectedIndex = -1;
            gateModelComboBox.SelectedIndex = -1;
            gateColorTextBox.Text = "";
            installationAfterDatePicker.SelectedDate = null;
            gatenumberTextBox.Text = "";
            searchTerms = new CustomerSearchTerms();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            searchTerms.Code = codeTextBox.Text;
            searchTerms.Name = nameTextBox.Text;
            searchTerms.Address = addressTextBox.Text;
            searchTerms.GateColor = gateColorTextBox.Text;
            searchTerms.GateNumber = gatenumberTextBox.Text;
            if (zipcodeComboBox.SelectedIndex > -1)
                searchTerms.City = (City)cityComboBox.SelectedItem;
            if (gateModelComboBox.SelectedIndex > -1)
                searchTerms.GateModel = (GateModel)gateModelComboBox.SelectedItem;
            if (installationAfterDatePicker.SelectedDate != null)
                searchTerms.InstalationAfter = (DateTime)installationAfterDatePicker.SelectedDate;
            DialogResult = true;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            City_services cService = new City_services();
            List<City> cities = cService.GetAll();
            List<City> zipcodes = new List<City>();
            foreach (City zip in cities)
            {
                bool gevonden = false;
                foreach (City city in zipcodes)
                {
                    if (zip.Zipcode == city.Zipcode)
                        gevonden = true;
                }
                if (!gevonden)
                    zipcodes.Add(zip);
            }
            zipcodes = zipcodes.OrderBy(z => z.Zipcode).ToList();
            City empty = new City();
            empty.Zipcode = " ";
            zipcodes.Insert(0, empty);
            zipcodeComboBox.ItemsSource = zipcodes;
            zipcodeComboBox.DisplayMemberPath = "Zipcode";

            cityComboBox.ItemsSource = new City_services().GetAll();
            cityComboBox.DisplayMemberPath = "Name";

            Gatemodel_services gmService = new Gatemodel_services();
            List<GateModel> gatemodels = gmService.GetAll();
            gateModelComboBox.ItemsSource = gatemodels;
            gateModelComboBox.DisplayMemberPath = "Model";

            codeTextBox.Focus();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                Search();
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void zipcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void zipcodeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            SetCities();
        }

        private void SetCities()
        {
            if (zipcodeComboBox.SelectedIndex > 0)
            {
                List<City> cities = new City_services().GetByZipcode(((City)zipcodeComboBox.SelectedItem).Zipcode);
                cityComboBox.ItemsSource = cities;
                cityComboBox.DisplayMemberPath = "Name";
                cityComboBox.SelectedIndex = 0;
            }
            else
            {
                cityComboBox.ItemsSource = new City_services().GetAll();
                cityComboBox.DisplayMemberPath = "Name";
            }
        }

        private void cityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetZipCode();
        }

        private void SetZipCode()
        {
            if (cityComboBox.SelectedIndex > 0)
            {
                City city = (City)cityComboBox.SelectedItem;
                zipcodeComboBox.Text = city.Zipcode;
            }
        }
    }
}
