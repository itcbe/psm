﻿using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMWpf
{
    public class PSMRole_service
    {
        public ObservableCollection<PSMRole> GetAllObservable()
        {
            using (var entities = new PoortserviceEntities())
            {
                return new ObservableCollection<PSMRole>((from r in entities.PSMRole
                                                          where r.Active == true
                                                          select r).ToList());
            }
        }

        public List<PSMRole> GetAll()
        {
            using (var entities = new PoortserviceEntities())
            {
                return (from r in entities.PSMRole
                        where r.Active == true
                        orderby r.Name
                        select r).ToList();
            }
        }

        internal void Delete(PSMRole role)
        {
            role.Active = false;
            Update(role);
        }

        internal PSMRole Save(PSMRole role)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from r in entities.PSMRole
                        where r.Name.ToLower() == role.Name.ToLower()             
                        select r).FirstOrDefault();
                if (found == null)
                {
                    entities.PSMRole.Add(role);
                    entities.SaveChanges();
                    return role;
                }
                else
                {
                    if (found.Active == false)
                    {
                        found.Active = true;
                        entities.SaveChanges();
                        return found;
                    }
                    else
                        throw new Exception("De groep bestaat reeds!");
                }
            }
        }

        internal void Update(PSMRole role)
        {
            using (var entities = new PoortserviceEntities())
            {
                var found = (from r in entities.PSMRole
                             where r.Id == role.Id
                             select r).FirstOrDefault();
                if (found != null)
                {
                    found.Name = role.Name;
                    found.Active = role.Active;
                    found.Modified = role.Modified;
                    found.ModifiedBy = role.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De groep is niet gevonden!");
                }
            }
        }
    }
}
