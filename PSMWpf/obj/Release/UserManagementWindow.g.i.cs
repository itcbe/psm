﻿#pragma checksum "..\..\UserManagementWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4BF8B19B8980D4F081FDFD019D708BE8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using PSMData;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PSMWpf {
    
    
    /// <summary>
    /// UserManagementWindow
    /// </summary>
    public partial class UserManagementWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl UserGroupTabControl;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid pSMUserDataGrid;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn userNameColumn;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn firstNameColumn;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn lastNameColumn;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid1;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox firstNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lastNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox userNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox languageComboBox;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox passwordTextBox;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox comfirmPasswordPasswordBox;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox emailTextBox;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox phoneTextBox;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox pSMRoleComboBox;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox localWorkDirTextBox;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox enabledCheckBox;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CloseButton;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveButton;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NewButton;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteButton;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid pSMRoleDataGrid;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn nameColumn;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid2;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nameTextBox;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GroupCloseButton;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GroupSaveButton;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GroupNewButton;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\UserManagementWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GroupDeleteButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PSMWpf;component/usermanagementwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\UserManagementWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 5 "..\..\UserManagementWindow.xaml"
            ((PSMWpf.UserManagementWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded_1);
            
            #line default
            #line hidden
            
            #line 5 "..\..\UserManagementWindow.xaml"
            ((PSMWpf.UserManagementWindow)(target)).KeyUp += new System.Windows.Input.KeyEventHandler(this.Window_KeyUp_1);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 18 "..\..\UserManagementWindow.xaml"
            ((System.Windows.Controls.Label)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Label_MouseLeftButtonDown_1);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 19 "..\..\UserManagementWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CloseButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.UserGroupTabControl = ((System.Windows.Controls.TabControl)(target));
            return;
            case 5:
            this.pSMUserDataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 30 "..\..\UserManagementWindow.xaml"
            this.pSMUserDataGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.pSMUserDataGrid_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.userNameColumn = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 7:
            this.firstNameColumn = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 8:
            this.lastNameColumn = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 9:
            this.grid1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.firstNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.lastNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.userNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.languageComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 77 "..\..\UserManagementWindow.xaml"
            this.languageComboBox.DropDownClosed += new System.EventHandler(this.languageComboBox_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 14:
            this.passwordTextBox = ((System.Windows.Controls.PasswordBox)(target));
            
            #line 85 "..\..\UserManagementWindow.xaml"
            this.passwordTextBox.LostFocus += new System.Windows.RoutedEventHandler(this.passwordTextBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 15:
            this.comfirmPasswordPasswordBox = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 16:
            this.emailTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.phoneTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.pSMRoleComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 93 "..\..\UserManagementWindow.xaml"
            this.pSMRoleComboBox.DropDownClosed += new System.EventHandler(this.pSMRoleComboBox_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 19:
            this.localWorkDirTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.enabledCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 21:
            this.CloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 105 "..\..\UserManagementWindow.xaml"
            this.CloseButton.Click += new System.Windows.RoutedEventHandler(this.CloseButton_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.SaveButton = ((System.Windows.Controls.Button)(target));
            
            #line 106 "..\..\UserManagementWindow.xaml"
            this.SaveButton.Click += new System.Windows.RoutedEventHandler(this.SaveButton_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.NewButton = ((System.Windows.Controls.Button)(target));
            
            #line 107 "..\..\UserManagementWindow.xaml"
            this.NewButton.Click += new System.Windows.RoutedEventHandler(this.NewButton_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.DeleteButton = ((System.Windows.Controls.Button)(target));
            
            #line 108 "..\..\UserManagementWindow.xaml"
            this.DeleteButton.Click += new System.Windows.RoutedEventHandler(this.DeleteButton_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.pSMRoleDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 26:
            this.nameColumn = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 27:
            this.grid2 = ((System.Windows.Controls.Grid)(target));
            return;
            case 28:
            this.nameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.GroupCloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 140 "..\..\UserManagementWindow.xaml"
            this.GroupCloseButton.Click += new System.Windows.RoutedEventHandler(this.CloseButton_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.GroupSaveButton = ((System.Windows.Controls.Button)(target));
            
            #line 141 "..\..\UserManagementWindow.xaml"
            this.GroupSaveButton.Click += new System.Windows.RoutedEventHandler(this.SaveGroupButton_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.GroupNewButton = ((System.Windows.Controls.Button)(target));
            
            #line 142 "..\..\UserManagementWindow.xaml"
            this.GroupNewButton.Click += new System.Windows.RoutedEventHandler(this.NewGroupButton_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.GroupDeleteButton = ((System.Windows.Controls.Button)(target));
            
            #line 143 "..\..\UserManagementWindow.xaml"
            this.GroupDeleteButton.Click += new System.Windows.RoutedEventHandler(this.DeleteGroupButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

