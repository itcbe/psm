﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for EngineTypeManagementWindow.xaml
    /// </summary>
    public partial class EngineTypeManagementWindow : Window
    {
        private System.Windows.Data.CollectionViewSource engineTypeViewSource;
        private ObservableCollection<EngineType> enginetypes;
        private PSMUser currentUser;

        public EngineTypeManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            engineTypeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("engineTypeViewSource")));
            ReloadEngineTypes();
            
        }

        private void ReloadEngineTypes()
        {
            EngineType_services eService = new EngineType_services();
            enginetypes = eService.GetAllObservable();
            engineTypeViewSource.Source = enginetypes;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<EngineType>(from c in enginetypes
                                                                 where c.Name.ToLower().StartsWith(searchString.ToLower())
                                                                 orderby c.Name
                                                                 select c).ToList();

                engineTypeViewSource.Source = gevonden;
            }
            else
                engineTypeViewSource.Source = enginetypes;
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (engineTypeDataGrid.SelectedIndex > -1)
            {
                    EngineType type = (EngineType)engineTypeDataGrid.SelectedItem;
                    if (MessageBox.Show("Weet u zeker dat u het motortype " + type.Name + " wilt verwijderen!", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        EngineType_services eService = new EngineType_services();
                        eService.Delete(type);
                        ReloadEngineTypes();
                        MessageBox.Show("Motortype verwijderd.", "Motortype verwijderen");
                    }
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (engineTypeDataGrid.SelectedIndex > -1)
            {
                EngineType enginetype = (EngineType)engineTypeDataGrid.SelectedItem;
                if (enginetype.Id == 0)
                {
                    int i = 0;
                    foreach (EngineType item in enginetypes)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    enginetypes.RemoveAt(i);
                    engineTypeViewSource.Source = null;
                    engineTypeViewSource.Source = enginetypes;
                }
            }
        }


        private void SaveData()
        {
            if (engineTypeDataGrid.SelectedIndex > -1)
            {
                EngineType type = (EngineType)engineTypeDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateEngineType(type);
                if (valid.IsValid)
                {
                    EngineType_services eService = new EngineType_services();
                    if (type.Id == 0)
                    {
                        type.Active = true;
                        type.Created = DateTime.Now;
                        type.CreatedBy = currentUser.FirstName;
                        type.Modified = type.Created;
                        type.ModifiedBy = type.CreatedBy;
                        try
                        {
                            EngineType saved = eService.Save(type);
                            ReloadEngineTypes();
                            MessageBox.Show("Motortype opgeslagen.", "Motortype opslaan");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        type.Modified = DateTime.Now;
                        type.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            eService.Update(type);
                            ReloadEngineTypes();
                            MessageBox.Show("Motortype opgeslagen.", "Motortype opslaan");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }

        private void NewElement()
        {
            EngineType newType = new EngineType();
            newType.Id = 0;
            enginetypes.Add(newType);
            engineTypeViewSource.Source = null;
            engineTypeViewSource.Source = enginetypes;
            engineTypeDataGrid.SelectedIndex = enginetypes.Count - 1;
            engineTypeDataGrid.ScrollIntoView(newType);
        }
    }
}
