﻿using Microsoft.Win32;
using PSMData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for TemplateDocumentManagementWindow.xaml
    /// </summary>
    public partial class TemplateDocumentManagementWindow : Window
    {
        private PSMUser currentUser;
        private List<Template_Document> docs;

        public TemplateDocumentManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            Template_service tService = new Template_service();
            List<Template> templates = tService.GetAll();
            templateComboBox.ItemsSource = templates;
            templateComboBox.DisplayMemberPath = "TemplateType";

            System.Windows.Data.CollectionViewSource template_DocumentViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("template_DocumentViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // template_DocumentViewSource.Source = [generic data source]

            SetButtons();
        }

        private void SetButtons()
        {
            if (NLComboBox.SelectedIndex == -1)
                OpenNLButton.IsEnabled = false;
            else
                OpenNLButton.IsEnabled = true;

            if (FRComboBox.SelectedIndex == -1)
                OpenFRButton.IsEnabled = false;
            else
                OpenFRButton.IsEnabled = true;

            if (ENComboBox.SelectedIndex == -1)
                OpenENButton.IsEnabled = false;
            else
                OpenENButton.IsEnabled = true;

            if (DEComboBox.SelectedIndex == -1)
                OpenDEButton.IsEnabled = false;
            else
                OpenDEButton.IsEnabled = true;

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void SaveTemplateDocument(Template_Document templdoc)
        {
            Template_Document_service tdService = new Template_Document_service();
            try
            {
                tdService.Save(templdoc);
                MessageBox.Show("Sjaboon opgeslagen.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void templateComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Template template = (Template)box.SelectedItem;
                Document_service dservice = new Document_service();
                List<Document> allDocs = dservice.GetAll();
                SetLanguageComboBoxen(allDocs, template);

                Template_Document_service tdService = new Template_Document_service();
                docs = tdService.GetByTemplate(template);
                foreach (Template_Document item in docs)
                {
                    switch (item.Document1.Language1.Code)
                    {
                        case "NL":
                            NLComboBox.Text = item.Document1.Url;
                            break;
                        case "FR":
                            FRComboBox.Text = item.Document1.Url;
                            break;
                        case "EN":
                            ENComboBox.Text = item.Document1.Url;
                            break;
                        case "DE":
                            DEComboBox.Text = item.Document1.Url;
                            break;
                    }
                }
            }
            SetButtons();
        }

        private void SetLanguageComboBoxen(List<Document> docs, Template template)
        {
            List<Document> NLDocs = new List<Document>();
            List<Document> FRDocs = new List<Document>();
            List<Document> ENDocs = new List<Document>();
            List<Document> DEDocs = new List<Document>();

            foreach (Document doc in docs)
            {
                switch (doc.Language1.Code)
                {
                    case "NL":
                        NLDocs.Add(doc);
                        break;
                    case "FR":
                        FRDocs.Add(doc);
                        break;
                    case "EN":
                        ENDocs.Add(doc);
                        break;
                    case "DE":
                        DEDocs.Add(doc);
                        break;
                }
            }
            NLComboBox.ItemsSource = NLDocs;
            NLComboBox.DisplayMemberPath = "Url";

            FRComboBox.ItemsSource = FRDocs;
            FRComboBox.DisplayMemberPath = "Url";

            ENComboBox.ItemsSource = ENDocs;
            ENComboBox.DisplayMemberPath = "Url";

            DEComboBox.ItemsSource = DEDocs;
            DEComboBox.DisplayMemberPath = "Url";
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
        }

        private void SaveData()
        {
            Template template = (Template)templateComboBox.SelectedItem;
            if (NLComboBox.SelectedIndex > -1)
            {
                Document nlDoc = (Document)NLComboBox.SelectedItem;
                Template_Document templdoc = new Template_Document();
                templdoc.Document = nlDoc.Id;
                templdoc.Document1 = nlDoc;
                templdoc.Template = template.Id;
                templdoc.Created = DateTime.Now;
                templdoc.CreatedBy = currentUser.FirstName;
                templdoc.Modified = templdoc.Created;
                templdoc.ModifiedBy = templdoc.CreatedBy;
                SaveTemplateDocument(templdoc);
            }
            if (FRComboBox.SelectedIndex > -1)
            {
                Document frDoc = (Document)FRComboBox.SelectedItem;
                Template_Document templdoc = new Template_Document();
                templdoc.Document = frDoc.Id;
                templdoc.Document1 = frDoc;
                templdoc.Template = template.Id;
                templdoc.Created = DateTime.Now;
                templdoc.CreatedBy = currentUser.FirstName;
                templdoc.Modified = templdoc.Created;
                templdoc.ModifiedBy = templdoc.CreatedBy;
                SaveTemplateDocument(templdoc);

            }
            if (ENComboBox.SelectedIndex > -1)
            {
                Document enDoc = (Document)ENComboBox.SelectedItem;
                Template_Document templdoc = new Template_Document();
                templdoc.Document = enDoc.Id;
                templdoc.Document1 = enDoc;
                templdoc.Template = template.Id;
                templdoc.Created = DateTime.Now;
                templdoc.CreatedBy = currentUser.FirstName;
                templdoc.Modified = templdoc.Created;
                templdoc.ModifiedBy = templdoc.CreatedBy;
                SaveTemplateDocument(templdoc);
            }
            if (DEComboBox.SelectedIndex > -1)
            {
                Document deDoc = (Document)DEComboBox.SelectedItem;
                Template_Document templdoc = new Template_Document();
                templdoc.Document = deDoc.Id;
                templdoc.Document1 = deDoc;
                templdoc.Template = template.Id;
                templdoc.Created = DateTime.Now;
                templdoc.CreatedBy = currentUser.FirstName;
                templdoc.Modified = templdoc.Created;
                templdoc.ModifiedBy = templdoc.CreatedBy;
                SaveTemplateDocument(templdoc);
            }
        }

        private void OpenNLButton_Click(object sender, RoutedEventArgs e)
        {
            if (NLComboBox.SelectedIndex > -1)
            {
                string url = ((Document)NLComboBox.SelectedItem).Url;
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kan het bestand niet openen!");
                }
            }
        }

        private void OpenFRButton_Click(object sender, RoutedEventArgs e)
        {
            if (FRComboBox.SelectedIndex > -1)
            {
                string url = ((Document)FRComboBox.SelectedItem).Url;
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kan het bestand niet openen!");
                }
            }
        }

        private void OpenENButton_Click(object sender, RoutedEventArgs e)
        {
            if (ENComboBox.SelectedIndex > -1)
            {
                string url = ((Document)ENComboBox.SelectedItem).Url;
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kan het bestand niet openen!");
                }
            }
        }

        private void OpenDEButton_Click(object sender, RoutedEventArgs e)
        {
            if (DEComboBox.SelectedIndex > -1)
            {
                string url = ((Document)DEComboBox.SelectedItem).Url;
                try
                {
                    System.Diagnostics.Process.Start(url);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kan het bestand niet openen!");
                }
            }
        }
    }
}
