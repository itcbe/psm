﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for InterventionTypeManagementWindow.xaml
    /// </summary>
    public partial class InterventionTypeManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource interventionTypeViewSource;
        private ObservableCollection<InterventionType> interventionTypes;

        public InterventionTypeManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<InterventionType>(from i in interventionTypes
                                                                          where i.Name.ToLower().StartsWith(searchString.ToLower())
                                                                          orderby i.Name
                                                                          select i).ToList();

                interventionTypeViewSource.Source = gevonden;
            }
            else
                interventionTypeViewSource.Source = interventionTypes;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (interventionTypeDataGrid.SelectedIndex > -1)
            {
                InterventionType type = (InterventionType)interventionTypeDataGrid.SelectedItem;
                if (MessageBox.Show("Weet u zeker dat u het interventietype " + type.Name + " wilt verwijderen!", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    InterventionType_services iService = new InterventionType_services();
                    iService.Delete(type);
                    ReloadInterventionTypes();
                    MessageBox.Show("Interventietype verwijderd.", "Interventietype verwijderen");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            interventionTypeViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("interventionTypeViewSource")));
            ReloadInterventionTypes();
        }

        private void ReloadInterventionTypes()
        {
            InterventionType_services iService = new InterventionType_services();
            interventionTypes = iService.GetAllObservable();
            interventionTypeViewSource.Source = interventionTypes;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (interventionTypeDataGrid.SelectedIndex > -1)
            {
                InterventionType type = (InterventionType)interventionTypeDataGrid.SelectedItem;
                if (type.Id == 0)
                {
                    int i = 0;
                    foreach (InterventionType item in interventionTypes)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    interventionTypes.RemoveAt(i);
                    interventionTypeViewSource.Source = null;
                    interventionTypeViewSource.Source = interventionTypes;
                }
            }
        }


        private void NewElement()
        {
            InterventionType newType = new InterventionType();
            newType.Id = 0;
            interventionTypes.Add(newType);
            interventionTypeViewSource.Source = null;
            interventionTypeViewSource.Source = interventionTypes;
            interventionTypeDataGrid.SelectedIndex = interventionTypes.Count - 1;
            interventionTypeDataGrid.ScrollIntoView(newType);
        }

        private void SaveData()
        {
            if (interventionTypeDataGrid.SelectedIndex > -1)
            {
                InterventionType type = (InterventionType)interventionTypeDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateInterventionType(type);
                if (valid.IsValid)
                {
                    InterventionType_services iService = new InterventionType_services();
                    if (type.Id == 0)
                    {
                        type.Active = true;
                        type.Created = DateTime.Now;
                        type.CreatedBy = currentUser.FirstName;
                        type.Modified = type.Created;
                        type.ModifiedBy = type.CreatedBy;
                        try
                        {
                            InterventionType saved = iService.Save(type);
                            ReloadInterventionTypes();
                            MessageBox.Show("Interventietype opgeslagen.", "Interventietype opslaan");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        type.Modified = DateTime.Now;
                        type.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            iService.Update(type);
                            ReloadInterventionTypes();
                            MessageBox.Show("Interventietype opgeslagen.", "Interventietype opslaan");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
