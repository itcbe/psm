﻿using PSMData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PSMWpf
{
    partial class MainWindow : Window
    {
        #region menuclick events

        /// <summary>
        /// Landen Menu klik
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            CountryManagementWindow CW = new CountryManagementWindow();
            if (CW.ShowDialog() == true)
            {
                LoadCountryCombobox();
            }
        }

        private void LoadCountryCombobox()
        {
            //throw new NotImplementedException();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            EngineTypeManagementWindow EW = new EngineTypeManagementWindow(currentUser);
            if (EW.ShowDialog() == true)
            {
                string currentText = engineTypeComboBox.Text;
                LoadEngineTypeComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    engineTypeComboBox.Text = currentText;
            }
        }

        private void LoadEngineTypeComboBox()
        {
            EngineType_services etService = new EngineType_services();
            List<EngineType> enginetypes = etService.GetAll();
            enginetypes.Insert(0, new EngineType());
            engineTypeComboBox.ItemsSource = enginetypes;
            engineTypeComboBox.DisplayMemberPath = "Name";
        }

        private void GateDecorationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GateDecorations DW = new GateDecorations(currentUser);
            if (DW.ShowDialog() == true)
            {
                string currentText = gateDecorationComboBox.Text;
                LoadGateDecorationComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    gateDecorationComboBox.Text = currentText;
            }
        }

        private void LoadGateDecorationComboBox()
        {
            Gate_Decoration_services gService = new Gate_Decoration_services();
            List<GateDecoration> gatedecorations = gService.GetAll();
            gatedecorations.Insert(0, new GateDecoration());
            gateDecorationComboBox.ItemsSource = gatedecorations;
            gateDecorationComboBox.DisplayMemberPath = "Name";
        }

        private void GateDriverMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GateDriverManagementWindow GW = new GateDriverManagementWindow(currentUser);
            if (GW.ShowDialog() == true)
            {
                string currentText = gateDriverComboBox.Text;
                LoadGateDriverComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    gateDriverComboBox.Text = currentText;
            }
        }

        private void LoadGateDriverComboBox()
        {
            GateDriver_services gdService = new GateDriver_services();
            List<GateDriver> gatedrivers = gdService.GetAll();
            gatedrivers.Insert(0, new GateDriver());
            gateDriverComboBox.ItemsSource = gatedrivers;
            gateDriverComboBox.DisplayMemberPath = "Name";

        }

        private void GateModelMenuItem_Click(object sender, RoutedEventArgs e)
        {
            GateModelManagementWindow MW = new GateModelManagementWindow(currentUser);
            if (MW.ShowDialog() == true)
            {
                string currentText = gateModelComboBox.Text;
                LoadGateModelComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    gateModelComboBox.Text = currentText;
            }
        }

        private void LoadGateModelComboBox()
        {
            Gatemodel_services gmService = new Gatemodel_services();
            List<GateModel> gateModels = gmService.GetAll();
            gateModels.Insert(0, new GateModel());
            gateModelComboBox.ItemsSource = gateModels;
            gateModelComboBox.DisplayMemberPath = "Model";

        }

        private void LegalCapacityMenuItem_Click(object sender, RoutedEventArgs e)
        {
            LegalFormManagementWindow LW = new LegalFormManagementWindow(currentUser);
            if (LW.ShowDialog() == true)
            {
                string currentText = legalCapacityComboBox.Text;
                LoadLegalCapacityComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    legalCapacityComboBox.Text = currentText;
            }
        }

        private void LoadLegalCapacityComboBox()
        {
            LegalCapacity_services lService = new LegalCapacity_services();
            List<LegalCapacity> legalCapacities = lService.GetAll();
            legalCapacities.Insert(0, new LegalCapacity());
            legalCapacityComboBox.ItemsSource = legalCapacities;
            legalCapacityComboBox.DisplayMemberPath = "Name";
        }

        private void MaintenanceFrequencyMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceFrequencyManagementWindow FW = new MaintenanceFrequencyManagementWindow(currentUser);
            if (FW.ShowDialog() == true)
            {
                string currentText = maintenanceFrequencyComboBox.Text;
                LoadMaintenanceFrequencyComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    maintenanceFrequencyComboBox.Text = currentText;
            }
        }

        private void LoadMaintenanceFrequencyComboBox()
        {
            Maintenance_frequency_services mfService = new Maintenance_frequency_services();
            List<MaintenanceFrequency> frequencies = mfService.GetAll();
            frequencies.Insert(0, new MaintenanceFrequency());
            maintenanceFrequencyComboBox.ItemsSource = frequencies;
            maintenanceFrequencyComboBox.DisplayMemberPath = "Frequency";
        }

        private void RailsystemMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RailsystemManagementWindow RW = new RailsystemManagementWindow(currentUser);
            if (RW.ShowDialog() == true)
            {
                string currentText = railSystemComboBox.Text;
                LoadRailsystemComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    railSystemComboBox.Text = currentText;
            }
        }

        private void LoadRailsystemComboBox()
        {
            Railsystem_services rService = new Railsystem_services();
            List<RailSystem> railsystems = rService.GetAll();
            railsystems.Insert(0, new RailSystem());
            railSystemComboBox.ItemsSource = railsystems;
            railSystemComboBox.DisplayMemberPath = "Name";
        }

        private void RemoteControlMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RemotecontrolManagementWindow CW = new RemotecontrolManagementWindow(currentUser);
            if (CW.ShowDialog() == true)
            {
                string currentText = remoteControlComboBox.Text;
                LoadRemoteControlComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    remoteControlComboBox.Text = currentText;
            }
        }

        private void LoadRemoteControlComboBox()
        {
            RemoteControl_services rcService = new RemoteControl_services();
            List<RemoteControl> remotecontrols = rcService.GetAll();
            remotecontrols.Insert(0, new RemoteControl());
            remoteControlComboBox.ItemsSource = remotecontrols;
            remoteControlComboBox.DisplayMemberPath = "Name";
        }

        private void SafetyMeasureMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SafetyMaesureManagementWindow SW = new SafetyMaesureManagementWindow(currentUser);
            if (SW.ShowDialog() == true)
            {
                //reload safetymeasure listbox
            }
        }

        private void ServiceInstallationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ServiceInstallationManagementWindow IW = new ServiceInstallationManagementWindow(currentUser);
            if (IW.ShowDialog() == true)
            {
                //reload serviceinstallations listbox
            }
        }

        private void VoltageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            VoltageManagmentWindow VW = new VoltageManagmentWindow(currentUser);
            if (VW.ShowDialog() == true)
            {
                string currentText = voltageComboBox.Text;
                LoadVoltageComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    voltageComboBox.Text = currentText;
            }
        }

        private void LoadVoltageComboBox()
        {
            Voltage_services vService = new Voltage_services();
            List<Voltage> voltages = vService.GetAll();
            voltages.Insert(0, new Voltage());
            voltageComboBox.ItemsSource = voltages;
            voltageComboBox.DisplayMemberPath = "Amount";
        }

        private void CityMenuItem_Click(object sender, RoutedEventArgs e)
        {
            CityManagementWindow CW = new CityManagementWindow(currentUser);
            if (CW.ShowDialog() == true)
            {
                string currentText = zipcodeComboBox.Text;
                LoadCityComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    zipcodeComboBox.Text = currentText;
            }
        }

        private void LoadCityComboBox()
        {
            City_services cityService = new City_services();
            List<City> cities = cityService.GetAll();
            List<City> zipcodes = new List<City>();

            foreach (City zip in cities)
            {
                bool gevonden = false;
                foreach (City city in zipcodes)
                {
                    if (zip.Zipcode == city.Zipcode)
                        gevonden = true;
                }
                if (!gevonden)
                    zipcodes.Add(zip);
            }

            cities = cities.OrderBy(c => c.Name).ToList();
            zipcodes = zipcodes.OrderBy(z => z.Zipcode).ToList();
            City empty = new City();
            empty.Zipcode = " ";
            zipcodes.Insert(0, empty);
            zipcodeComboBox.ItemsSource = zipcodes;
            zipcodeComboBox.DisplayMemberPath = "Zipcode";

            CityComboBox.ItemsSource = cityService.GetAll();
            CityComboBox.DisplayMemberPath = "Name";
        }

        private void InterventionTypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            InterventionTypeManagementWindow ITW = new InterventionTypeManagementWindow(currentUser);
            if (ITW.ShowDialog() == true)
            {
                string currentText = interventionTypeComboBox.Text;
                LoadInterventionTypeComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    interventionTypeComboBox.Text = currentText;
            }
        }

        private void LoadInterventionTypeComboBox()
        {
            InterventionType_services itService = new InterventionType_services();
            List<InterventionType> interventiontypes = itService.GetAll();
            interventiontypes.Insert(0, new InterventionType());
            interventionTypeComboBox.ItemsSource = interventiontypes;
            interventionTypeComboBox.DisplayMemberPath = "Name";

        }

        private void LanguageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            LanguageManagementWindow LW = new LanguageManagementWindow(currentUser);
            if (LW.ShowDialog() == true)
            {
                string currentText = languageComboBox.Text;
                LoadLanguageComboBox();
                if (!string.IsNullOrEmpty(currentText))
                    languageComboBox.Text = currentText;
            }
        }

        private void LoadLanguageComboBox()
        {
            Language_services languageService = new Language_services();
            List<Language> languages = languageService.GetAll();
            languages.Insert(0, new Language());
            languageComboBox.ItemsSource = languages;
            languageComboBox.DisplayMemberPath = "Name";
        }

        private void MaintenanceOverviewMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceOverviewWindow MW = new MaintenanceOverviewWindow(currentUser);
            MW.Show();
        }

        private void PSMUserMenuItem_Click(object sender, RoutedEventArgs e)
        {
            UserManagementWindow UW = new UserManagementWindow(currentUser);
            if (UW.ShowDialog() == true)
            {

            }
        }

        #endregion

    }
}
