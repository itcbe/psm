﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for UserManagementWindow.xaml
    /// </summary>
    public partial class UserManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource pSMUserViewSource;
        private System.Windows.Data.CollectionViewSource pSMRoleViewSource;
        private ObservableCollection<PSMUser> users;
        private ObservableCollection<PSMRole> roles;

        public UserManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            GetLanguages();
            GetGroups();
            pSMRoleViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("pSMRoleViewSource")));
            ReloadGroups();

            pSMUserViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("pSMUserViewSource")));
            ReloadUsers();
        }

        private void GetGroups()
        {
            PSMRole_service rService = new PSMRole_service();
            List<PSMRole> comboroles = rService.GetAll();
            pSMRoleComboBox.ItemsSource = comboroles;
            pSMRoleComboBox.DisplayMemberPath = "Name";
        }

        private void ReloadGroups()
        {
            PSMRole_service rService = new PSMRole_service();
            roles = rService.GetAllObservable();
            pSMRoleViewSource.Source = roles;
        }

        private void GetLanguages()
        {
            Language_services lService = new Language_services();
            List<Language> languages = lService.GetAll();
            languageComboBox.ItemsSource = languages;
            languageComboBox.DisplayMemberPath = "Name";
        }

        private void ReloadUsers()
        {
            PSMUser_service uService = new PSMUser_service();
            users = uService.GetAllObservable();
            pSMUserViewSource.Source = users;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveUserData();
        }

        private void ClearPasswordBox()
        {
            passwordTextBox.Password = "";
            comfirmPasswordPasswordBox.Password = "";
        }

        private void passwordTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox box = (PasswordBox)sender;
            String pw = box.Password;
            if (!string.IsNullOrEmpty(pw))
            {
                PSMUser user = (PSMUser)pSMUserDataGrid.SelectedItem;
                if (user != null)
                {
                    if (user.Password != pw)
                    {
                        if (user.Id != 0)
                        {
                            if (MessageBox.Show("Uw geeft een ander paswoord in. Wil u het paswoord wijzigen?", "Opgelet!!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            {
                                user.Password = pw;
                            }
                            else
                            {
                                passwordTextBox.Password = "";
                                passwordTextBox.Focus();
                            }
                        }
                        else
                            user.Password = pw;
                    }
                }
            }
        }


        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewUserElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (pSMUserDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de gebruiker verwijderen?", "Verwijderen gebruiker", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    PSMUser user = (PSMUser)pSMUserDataGrid.SelectedItem;
                    PSMUser_service uService = new PSMUser_service();
                    user.Modified = DateTime.Now;
                    user.ModifiedBy = currentUser.FirstName;
                    uService.Delete(user);
                    ReloadUsers();
                    MessageBox.Show("Gebruiker verwijderd.", "Verwijderen verwijderen");
                }
            }
        }

        private void pSMUserDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg.SelectedIndex > -1)
            {
                PSMUser user = (PSMUser)dg.SelectedItem;
                try
                {
                    languageComboBox.Text = user.Language1.Name;
                }
                catch (Exception)
                {
                    languageComboBox.SelectedIndex = -1;
                }
                try
                {
                    pSMRoleComboBox.Text = user.PSMRole1.Name;
                }
                catch (Exception)
                {
                    pSMRoleComboBox.SelectedIndex = -1;
                }
            }
        }

        private void SaveGroupButton_Click(object sender, RoutedEventArgs e)
        {
            SaveGroupData();
        }

        private void NewGroupButton_Click(object sender, RoutedEventArgs e)
        {
            NewGroupElement();
        }

        private void DeleteGroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (pSMRoleDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de groep verwijderen?", "Verwijderen groep", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    PSMRole role = (PSMRole)pSMRoleDataGrid.SelectedItem;
                    PSMRole_service rService = new PSMRole_service();
                    role.Modified = DateTime.Now;
                    role.ModifiedBy = currentUser.FirstName;
                    rService.Delete(role);
                    ReloadGroups();
                    MessageBox.Show("Groep verwijderd.", "Verwijderen groep");
                }
            }
        }

        private void languageComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                if (pSMUserDataGrid.SelectedIndex > -1)
                {
                    Language language = (Language)box.SelectedItem;
                    PSMUser user = (PSMUser)pSMUserDataGrid.SelectedItem;
                    user.Language = language.Id;
                    user.Language1 = language;
                }

            }
        }

        private void pSMRoleComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                if (pSMUserDataGrid.SelectedIndex > -1)
                {
                    PSMRole role = (PSMRole)box.SelectedItem;
                    PSMUser user = (PSMUser)pSMUserDataGrid.SelectedItem;
                    user.PSMRole = role.Id;
                    user.PSMRole1 = role;
                }

            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (((TabItem)UserGroupTabControl.SelectedItem).Header.ToString() == "Gebruikers")
                    SaveUserData();
                else
                    SaveGroupData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (((TabItem)UserGroupTabControl.SelectedItem).Header.ToString() == "Gebruikers")
                    NewUserElement();
                else
                    NewGroupElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            TabItem tab = (TabItem)UserGroupTabControl.SelectedItem;
            switch (tab.Header.ToString())
            {
                case "Gebruikers":
                    RemoveEmptyUser();
                    break;
                case "Gebruikersgroep":
                    RemoveEmptyGroup();
                    break;
                default:
                    break;
            }
        }

        private void RemoveEmptyGroup()
        {
            if (pSMRoleDataGrid.SelectedIndex > -1)
            {
                PSMRole role = (PSMRole)pSMRoleDataGrid.SelectedItem;
                if (role.Id == 0)
                {
                    int i = 0;
                    foreach (PSMRole item in roles)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    roles.RemoveAt(i);
                    pSMRoleViewSource.Source = null;
                    pSMRoleViewSource.Source = roles;
                }
            }
        }

        private void RemoveEmptyUser()
        {
            if (pSMUserDataGrid.SelectedIndex > -1)
            {
                PSMUser user = (PSMUser)pSMUserDataGrid.SelectedItem;
                if (user.Id == 0)
                {
                    int i = 0;
                    foreach (PSMUser item in users)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    users.RemoveAt(i);
                    pSMUserViewSource.Source = null;
                    pSMUserViewSource.Source = users;
                }
            }
        }

        private void SaveGroupData()
        {
            if (pSMRoleDataGrid.SelectedIndex > -1)
            {
                PSMRole role = (PSMRole)pSMRoleDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateRole(role);
                if (valid.IsValid)
                {
                    PSMRole_service rService = new PSMRole_service();
                    if (role.Id == 0)
                    {
                        role.Active = true;
                        role.Created = DateTime.Now;
                        role.CreatedBy = currentUser.FirstName;
                        role.Modified = DateTime.Now;
                        role.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            PSMRole saved = rService.Save(role);
                            ReloadGroups();
                            MessageBox.Show("Groep opgeslagen.", "Opslaan Groep");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        role.Modified = DateTime.Now;
                        role.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            rService.Update(role);
                            ReloadGroups();
                            MessageBox.Show("Groep opgeslagen.", "Opslaan Groep");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    string currenttext = pSMRoleComboBox.Text;
                    GetGroups();
                    if (!string.IsNullOrEmpty(currenttext))
                        pSMRoleComboBox.Text = currenttext;

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }

        private void NewGroupElement()
        {
            PSMRole newRole = new PSMRole();
            newRole.Id = 0;
            roles.Add(newRole);
            pSMRoleViewSource.Source = null;
            pSMRoleViewSource.Source = roles;
            pSMRoleDataGrid.SelectedIndex = roles.Count - 1;
            pSMRoleDataGrid.ScrollIntoView(newRole);
        }

        private void SaveUserData()
        {
            if (pSMUserDataGrid.SelectedIndex > -1)
            {
                PSMUser user = (PSMUser)pSMUserDataGrid.SelectedItem;
                if (user.PSMRole1 == null)
                {
                    if (pSMRoleComboBox.SelectedIndex > -1)
                    {
                        PSMRole role = (PSMRole)pSMRoleComboBox.SelectedItem;
                        user.PSMRole = role.Id;
                        user.PSMRole1 = role;
                    }
                }
                if (user.Language1 == null)
                {
                    if (languageComboBox.SelectedIndex > -1)
                    {
                        Language lang = (Language)languageComboBox.SelectedItem;
                        user.Language = lang.Id;
                        user.Language1 = lang;
                    }
                }
                Valid valid = OurValidation.ValidateUser(user, comfirmPasswordPasswordBox.Password);
                if (valid.IsValid)
                {
                    PSMUser_service uService = new PSMUser_service();
                    if (user.Id == 0)
                    {
                        user.PSMRole1 = null;
                        user.Language1 = null;
                        user.Active = true;
                        user.Enabled = true;
                        user.Created = DateTime.Now;
                        user.CreatedBy = currentUser.FirstName;
                        user.Modified = DateTime.Now;
                        user.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            PSMUser saved = uService.Save(user);
                            ReloadUsers();
                            MessageBox.Show("Gebruiker opgeslagen.", "Opslaan Gebruiker");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        user.Modified = DateTime.Now;
                        user.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            uService.Update(user);
                            ReloadUsers();
                            MessageBox.Show("Gebruiker opgeslagen.", "Opslaan Gebruiker");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    ClearPasswordBox();
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }

        private void NewUserElement()
        {
            PSMUser newUser = new PSMUser();
            newUser.Id = 0;
            users.Add(newUser);
            pSMUserViewSource.Source = null;
            pSMUserViewSource.Source = users;
            pSMUserDataGrid.SelectedIndex = users.Count - 1;
            pSMUserDataGrid.ScrollIntoView(newUser);
        }

    }
}
