﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for VoltageManagmentWindow.xaml
    /// </summary>
    public partial class VoltageManagmentWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource voltageViewSource;
        private ObservableCollection<Voltage> voltages;

        public VoltageManagmentWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<Voltage>((from s in voltages
                                                                  where s.Amount.ToString().Contains(searchString)
                                                                  select s).ToList());

                voltageViewSource.Source = gevonden;
            }
            else
                voltageViewSource.Source = voltages;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (voltageDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de aansluitspanning verwijderen?", "Verwijderen Aansluitspanning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Voltage voltage = (Voltage)voltageDataGrid.SelectedItem;
                    Voltage_services vService = new Voltage_services();
                    voltage.Modified = DateTime.Now;
                    voltage.ModifiedBy = currentUser.FirstName;
                    vService.Delete(voltage);
                    ReloadVoltages();
                    MessageBox.Show("Aansluitspanning verwijderd.", "Verwijderen Aansluitspanning");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            voltageViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("voltageViewSource")));
            ReloadVoltages();
        }

        private void ReloadVoltages()
        {
            Voltage_services vService = new Voltage_services();
            voltages = vService.GetAllObservable();
            voltageViewSource.Source = voltages;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (voltageDataGrid.SelectedIndex > -1)
            {
                Voltage voltage = (Voltage)voltageDataGrid.SelectedItem;
                if (voltage.Id == 0)
                {
                    int i = 0;
                    foreach (Voltage item in voltages)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    voltages.RemoveAt(i);
                    voltageViewSource.Source = null;
                    voltageViewSource.Source = voltages;
                }
            }
        }


        private void NewElement()
        {
            Voltage newVoltage = new Voltage();
            newVoltage.Id = 0;
            voltages.Add(newVoltage);
            voltageViewSource.Source = null;
            voltageViewSource.Source = voltages;
            voltageDataGrid.SelectedIndex = voltages.Count - 1;
            voltageDataGrid.ScrollIntoView(newVoltage);
        }

        private void SaveData()
        {
            if (voltageDataGrid.SelectedIndex > -1)
            {
                Voltage voltage = (Voltage)voltageDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateVoltage(voltage);
                if (valid.IsValid)
                {
                    Voltage_services vService = new Voltage_services();
                    if (voltage.Id == 0)
                    {
                        voltage.Active = true;
                        voltage.Created = DateTime.Now;
                        voltage.CreatedBy = currentUser.FirstName;
                        voltage.Modified = DateTime.Now;
                        voltage.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            Voltage saved = vService.Save(voltage);
                            ReloadVoltages();
                            MessageBox.Show("Aansluitspanning opgeslagen.", "Opslaan Aansluitspanning");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        voltage.Modified = DateTime.Now;
                        voltage.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            vService.Update(voltage);
                            ReloadVoltages();
                            MessageBox.Show("Aansluitspanning opgeslagen.", "Opslaan Aansluitspanning");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
