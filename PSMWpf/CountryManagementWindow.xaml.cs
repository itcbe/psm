﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PSMData;
using System.Collections.ObjectModel;
using PSMClassLibrary;
using ITCLibrary;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for CountryManagementWindow.xaml
    /// </summary>
    public partial class CountryManagementWindow : Window
    {
        private System.Windows.Data.CollectionViewSource countryViewSource;
        private ObservableCollection<Country> countries;

        public CountryManagementWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            countryViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("countryViewSource")));
            ReloadCountries();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (countryDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Weet u zeker dat u het land " + countryDataGrid.Name + " wilt verwijderen!", "", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Country country = (Country)countryDataGrid.SelectedItem;
                    Country_services cService = new Country_services();
                    cService.Delete(country);
                    ReloadCountries();
                }
            }
        }

        private void ReloadCountries()
        {
            Country_services cService = new Country_services();
            countries = cService.GetAllObservable();
            countryViewSource.Source = countries;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<Country>(from c in countries
                                                                 where c.Name.ToLower().StartsWith(searchString.ToLower())
                                                                 orderby c.Name
                                                                 select c).ToList();

                countryViewSource.Source = gevonden;
            }
            else
                countryViewSource.Source = countries;
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (countryDataGrid.SelectedIndex > -1)
            {
                Country country = (Country)countryDataGrid.SelectedItem;
                if (country.Id == 0)
                {
                    int i = 0;
                    foreach (Country item in countries)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    countries.RemoveAt(i);
                    countryViewSource.Source = null;
                    countryViewSource.Source = countries;
                }
            }
        }

        private void SaveData()
        {
            if (countryDataGrid.SelectedIndex > -1)
            {
                Country country = (Country)countryDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateCountry(country);
                if (valid.IsValid)
                {
                    Country_services cService = new Country_services();
                    if (country.Id == 0)
                    {
                        country.Active = true;
                        country.Created = DateTime.Now;
                        country.CreatedBy = "";
                        try
                        {
                            Country saved = cService.Save(country);
                            ReloadCountries();
                            MessageBox.Show("Land opgeslagen!", "Land opslaan");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        country.Modified = DateTime.Now;
                        country.ModifiedBy = "";
                        try
                        {
                            cService.Update(country);
                            ReloadCountries();
                            MessageBox.Show("Land opgeslagen!", "Land opslaan");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    System.Windows.MessageBox.Show(valid.Message);
            }
        }

        private void NewElement()
        {
            Country newCountry = new Country();
            newCountry.Id = 0;
            countries.Add(newCountry);
            countryViewSource.Source = null;
            countryViewSource.Source = countries;
            countryDataGrid.SelectedIndex = countries.Count - 1;
            countryDataGrid.ScrollIntoView(newCountry);
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
