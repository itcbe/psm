﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
using Microsoft.Win32;
using Excel = Microsoft.Office.Interop.Excel;

namespace ITCLibrary
{
    public class ExcelDocument
    {
        public Excel.Worksheet xlWorkSheet;
        public Excel.Workbook xlWorkBook;
        public Excel.Application xlApp;
        public Excel.Range rangeToHoldHyperlink;
        public Excel.Range CellInstance;
        public String fileName;
        
        public Excel.XlSaveAsAccessMode xlSaveAsAccessmode;
        private object misValue = System.Reflection.Missing.Value;
        
        public ExcelDocument()
        {                      
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlApp.DisplayAlerts = false;
            xlApp.Visible = true;
            //Dummy initialisation to prevent errors.
            rangeToHoldHyperlink = xlWorkSheet.get_Range("A1", Type.Missing);
            CellInstance = xlWorkSheet.get_Range("A1", Type.Missing);
        }

        public void SetColumnHeaders(String[] headers, int startpos, Boolean border, Boolean bold, Boolean altColor)
        {
            int pos = startpos;
            foreach (String header in headers)
            {
                if (altColor)
                    if ((pos % 2) == 1)
                        xlWorkSheet.Cells[1, pos].Interior.Color = System.Drawing.Color.LightBlue;
                if (bold)
                    xlWorkSheet.Cells[1,pos].Font.Bold = true;
                if (border)
                {
                    xlWorkSheet.Cells[1, pos].Borders.Color = System.Drawing.Color.Black;
                    xlWorkSheet.Cells[1, pos].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
                }
                xlWorkSheet.Cells[1, pos] = header;

                pos++;
            }
        }

        public void InsertList(String[][] items, Boolean border)
        {
            int colCounter = 1, rowcounter = 2;
            foreach (String[] myItem in items)
            {
                if (myItem != null)
                {
                    foreach (String item in myItem)
                    {
                        if (border)
                        {
                            xlWorkSheet.Cells[rowcounter, colCounter].Borders.Color = System.Drawing.Color.Black;
                            xlWorkSheet.Cells[rowcounter, colCounter].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
                        }
                        xlWorkSheet.Cells[rowcounter, colCounter] = item;
                        xlWorkSheet.Cells[rowcounter, colCounter].NumberFormat = "@";

                        xlWorkSheet.Cells[1, colCounter].EntireColumn.AutoFit();
                        //xlWorkSheet.Cells[rowcounter, 6].EntireColumn.NumberFormat = "d/mm/jjjj;@";
                        colCounter++;
                    }
                    colCounter = 1;
                    rowcounter++;
                }
            }
        }

        public void Save()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Nieuwe scheet";
            dlg.DefaultExt = ".xls";
            dlg.Filter = "Excelscheets | *.xls";

            if (dlg.ShowDialog() == true)
            {
                this.xlWorkBook.SaveAs(dlg.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                fileName = dlg.FileName;
            }
        }

        public void Close()
        {
            this.xlWorkBook.Close();
        }
    }
}
