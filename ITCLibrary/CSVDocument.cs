﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITCLibrary
{
    public class CSVDocument
    {
        private string _path = "";
        private StreamWriter writer;

        public string Path
        {
            get { return _path; }
            set { _path = value; }
        }

        public CSVDocument(string path)
        {
            Path = path;
            if (Path != "")
            {
                try
                {
                    writer = new StreamWriter(Path, false, System.Text.Encoding.UTF8);
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void AddRow(List<string> columns)
        {
            if (columns.Count > 0)
            {
                StringBuilder row = new StringBuilder();
                int index = 0;
                foreach (string item in columns)
                {
                    if (index == 0)
                        row.Append(item);
                    else
                        row.Append("; " + item);
                    index++;
                }
                writer.WriteLine(row.ToString());
            }
        }

        public void Save()
        {
            writer.Dispose();
        }
    }
}
