﻿//using PomsClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Controls;


namespace ITCLibrary
{
    public class ITCHelper
    {
        //Functie om de dag van de week op te vragen. returnt een String
        public static String getDagVanDeWeek(DayOfWeek dayOfTheWeek)
        {
            if (dayOfTheWeek.Equals(System.DayOfWeek.Monday))
                return "Maandag";
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Tuesday))
                return "Dinsdag";
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Wednesday))
                return "Woensdag";
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Thursday))
                return "Donderdag";
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Friday))
                return "Vrijdag";
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Saturday))
                return "Zaterdag";
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Sunday))
                return "Zondag";
            else
                return "Dag";
        }

        public static void SetTextBoxToNormal(System.Windows.Controls.TextBox myTextBox)
        {
            myTextBox.BorderThickness = new Thickness(2, 2, 1, 1);
            myTextBox.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 85, 85, 85));
        }

        public static void SetTextBoxInError(System.Windows.Controls.TextBox myTextBox)
        {
            myTextBox.BorderThickness = new Thickness(2, 2, 1, 1);
            myTextBox.BorderBrush = new SolidColorBrush(Colors.Red);
        }

        public static void SetComboBoxInError(System.Windows.Controls.ComboBox myComboBox)
        {
            myComboBox.BorderThickness = new Thickness(2, 2, 1, 1);
            myComboBox.BorderBrush = new SolidColorBrush(Colors.Red);
        }

        public static void SetComboBoxToNormal(System.Windows.Controls.ComboBox myComboBox)
        {
            myComboBox.BorderThickness = new Thickness(0);
            myComboBox.BorderBrush = new SolidColorBrush(Colors.Black);

        }

        public static void SetDatePickerInError(DatePicker myDatePicker)
        {
            myDatePicker.BorderThickness = new Thickness(1);
            myDatePicker.BorderBrush = new SolidColorBrush(Colors.Red);
        }

        public static void SetDatePickerToNormal(DatePicker myDatePicker)
        {
            myDatePicker.BorderThickness = new Thickness(1);
            myDatePicker.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 85, 85, 85));
        }



        //Functie om de maandnaam op te vragen. Returnt een String
        public static String getMaandVoluit(int maand)
        {
            String[] maanden = { "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december" };

            return maanden[maand - 1];
        }




        public static DateTime[] getWeek(DateTime datum)
        {
            DateTime[] week = new DateTime[7];

            int dagDatum = getIndexDagVanDeWeek(datum.DayOfWeek);

            //de geselecteerde datum
            week[dagDatum] = datum;

            //de dagen van de week voor de geselecteerde datum
            for (int i = dagDatum - 1; i >= 0; i--)
            {
                week[i] = datum.AddDays(-1 * (dagDatum - i));
            }

            //de dagen van de week na de geselecteerde datum
            for (int i = dagDatum + 1; i < 7; i++)
            {
                week[i] = datum.AddDays(i - dagDatum);
            }

            //print voor controle
            for (int i = 0; i < 7; i++)
                Console.WriteLine("Dag " + (i + 1) + ": " + week[i].ToString());

            return week;

        }

        private static int getIndexDagVanDeWeek(DayOfWeek dayOfTheWeek)
        {
            if (dayOfTheWeek.Equals(System.DayOfWeek.Monday))
                return 0;
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Tuesday))
                return 1;
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Wednesday))
                return 2;
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Thursday))
                return 3;
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Friday))
                return 4;
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Saturday))
                return 5;
            else if (dayOfTheWeek.Equals(System.DayOfWeek.Sunday))
                return 6;
            else
                return 0;
        }

        public static String getDatumVoluit(DateTime datum)
        {
            return ITCHelper.getDagVanDeWeek(datum.DayOfWeek) + " " + datum.Day + " " + ITCHelper.getMaandVoluit(datum.Month) + " " + datum.Year;
        }

        //Lijst van landen voor de landenCombobox
        public static String[] Landen = { " --- Kies uw land ---", "België", "Nederland", "Duitsland", "Luxemburg", "Frankrijk", "Engeland" };

        public static String[] Status = {" --- Maak een keuze ---", "Optie bij Poms", "Aangevraagd optie aan onderaannemer", "Aangevraagd offerte", "Bevestiging van optie aan Poms",
                                            "Bestelling bij Poms", "Aangevraagd besteling aan onderaannemer", "Bevestiging van bestelling aan Poms"};
                                        

        //Geeft de datum van de weekplanning buttonscaption terug. returnt een String
        public static String GetDatumVanString(String ButtonString)
        {
            
            String datum = ButtonString.Substring(3);
            return datum;
        }

        //kleurenlijst
        public static List<String> GetKleuren()
        {
            List<String> kleuren = new List<string>();
            kleuren.Add("LawnGreen");
            kleuren.Add("Plum");
            kleuren.Add("Aqua");
            kleuren.Add("Khaki");
            kleuren.Add("Magenta");
            kleuren.Add("LightSalmon");
            kleuren.Add("LightGray");

            return kleuren;
        }

    }
}
