﻿using PSMData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMClassLibrary
{
    public static class OurValidation
    {
        public static Valid ValidateCustomer(Customer customer)
        {
            Valid valid = new Valid();
            if (string.IsNullOrWhiteSpace(customer.Code))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen code ingegeven! Dit is een verplicht veld.";
            }
            if (string.IsNullOrWhiteSpace(customer.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen naam ingegeven! Dit is een verplicht veld.";
            }

            return valid;
        }

        public static Valid ValidateCustomerGate(CustomerGate gate)
        {
            Valid valid = new Valid();
            if (gate.Customer < 1)
            {
                valid.IsValid = false;
                valid.Message = "Geen klant gekozen!";
            }
            if (gate.GateModel < 1)
            {
                valid.IsValid = false;
                valid.Message += "\nGeen poortmodel gekozen!";
            }
            if (gate.Quantity < 1)
            {
                valid.IsValid = false;
                valid.Message += "\nGeen hoeveelheid ingegeven!";
            }

            return valid;
        }

        public static Valid ValidateIntervention(Intervention intervention)
        {
            Valid valid = new Valid();
            if (intervention.InterventionType < 1)
            {
                valid.IsValid = false;
                valid.Message = "Geen Type gekozen!";
            }
            if (string.IsNullOrEmpty(intervention.Description))
            {
                valid.IsValid = false;
                valid.Message += "\nGeen omschrijving ingegeven!";
            }
            if (intervention.PlannedDate == null)
            {
                valid.IsValid = false;
                valid.Message += "\nGeen geen planningsdatum ingegeven!";
            }

            return valid;
        }

        public static bool IsNaN(string number)
        {
            int num;
            if (int.TryParse(number, out num))
                return false;
            else
                return true;
        }

        public static Valid ValidateLanguage(Language language)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(language.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen taal ingegeven!\n";
            }
            if (string.IsNullOrEmpty(language.Code))
            {
                valid.IsValid = false;
                valid.Message += "Er is geen taalcode ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateRemoteControl(RemoteControl control)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(control.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen naam ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateInterventionType(InterventionType type)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(type.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen interventietype ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateEngineType(EngineType type)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(type.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen motortype ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateVoltage(Voltage voltage)
        {
            Valid valid = new Valid();
            if (voltage.Amount < 1)
            {
                valid.IsValid = false;
                valid.Message = "Er is geen geldige waarde ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateCity(City city)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(city.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen plaatsnaam ingegeven!\n";
            }
            if (string.IsNullOrEmpty(city.Zipcode))
            {
                valid.IsValid = false;
                valid.Message += "Er is geen postcode ingegeven!";
            }

            return valid;
        }

        public static Valid ValidateGateDecoration(GateDecoration decoration)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(decoration.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen beslag ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateGateDriver(GateDriver driver)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(driver.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen sturing ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateGateModel(GateModel model)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(model.Model))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen poortmodel ingegeven!\n";
            }
            if (model.GateBrand1 == null || model.GateBrand == 0)
            {
                valid.IsValid = false;
                valid.Message += "Er is geen merk gekozen!";
            }
            return valid;
        }

        public static Valid ValidateLegalCapacity(LegalCapacity legal)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(legal.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen hoedanigheid ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateMaintenanceFrequency(MaintenanceFrequency frequency)
        {
            Valid valid = new Valid();
            if (frequency.Frequency == 0)
            {
                valid.IsValid = false;
                valid.Message = "Er is geen geldige waarde ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateRailSystem(RailSystem system)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(system.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen railsysteem ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateSafetyMaesure(SafetyMeasure safety)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(safety.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen veiligheidsvoorzienning ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateServiceInstallation(ServiceInstallation installation)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(installation.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen bedieningsinstallatie ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateUser(PSMUser user,string confirmpassword)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(user.UserName))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen gebruikersnaam ingegeven!\n";
            }
            if (string.IsNullOrEmpty(user.FirstName))
            {
                valid.IsValid = false;
                valid.Message += "Er is geen voornaam ingegeven!\n";
            }
            if (string.IsNullOrEmpty(user.LastName))
            {
                valid.IsValid = false;
                valid.Message += "Er is geen achternaam ingegeven!\n";
            }
            if (string.IsNullOrEmpty(user.Password))
            {
                valid.IsValid = false;
                valid.Message += "Er is geen wachtwoord ingegeven!\n";
            }
            if (user.Password != confirmpassword)
            {
                valid.IsValid = false;
                valid.Message += "De wachtwoorden komen niet overeen!";
            }

            if (user.PSMRole1 == null)
            {
                valid.IsValid = false;
                valid.Message += "Geen groep gekozen!";
            }

            return valid;
        }

        public static Valid ValidateRole(PSMRole role)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(role.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen groepsnaam ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateTemplate(Template template)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(template.TemplateType))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen sjabloontype ingegeven!";
            }
            return valid;
        }

        public static Valid ValidateDocument(Document doc)
        {
            Valid valid = new Valid();
            if (string.IsNullOrEmpty(doc.Url))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen document gekozen!\n";
            }
            if (doc.Language1 == null || doc.Language == 0)
            {
                valid.IsValid = false;
                valid.Message += "Er is geen taal gekozen!";
            }
            return valid;
        }

        public static Valid ValidateDocumentCreation(Customer customer, CustomerGate gate, Template template)
        {
            Valid valid = new Valid();
            if (template == null)
            {
                valid.IsValid = false;
                valid.Message = "Er is geen sjabloon gekozen!\n";
            }
            if (customer == null)
            {
                valid.IsValid = false;
                valid.Message += "Er is geen klant gekozen!\n";
            }
            if (gate == null)
            {
                valid.IsValid = false;
                valid.Message += "Er is geen poort gekozen!\n";
            }

            return valid;
        }

        public static Valid ValidateCountry(Country country)
        {
            Valid valid = new Valid();
            if (string.IsNullOrWhiteSpace(country.Name))
            {
                valid.IsValid = false;
                valid.Message = "Er is geen land ingegeven!\n";
            }
            if (string.IsNullOrWhiteSpace(country.Code))
            {
                valid.IsValid = false;
                valid.Message += "Er is geen landcode ingegeven!\n";
            }

            return valid;
        }
    }
}
