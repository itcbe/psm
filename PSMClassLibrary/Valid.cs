﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMClassLibrary
{
    public class Valid
    {
        private bool _isvalid;

        public bool IsValid
        {
            get { return _isvalid; }
            set { _isvalid = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public Valid()
        {
            _isvalid = true;
            _message = "";
        }       
    }
}
