﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMClassLibrary
{
    public class PSMCheckBoxListBoxItem
    {
        public int id { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }

        public bool IsEnabled { get; set; }

    }
}
